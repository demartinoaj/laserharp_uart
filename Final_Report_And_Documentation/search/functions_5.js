var searchData=
[
  ['game_5fdrawmap',['Game_DrawMap',['../group__mike.html#ga356fa68055363ee50a26734ceac6683c',1,'Game_DrawMap():&#160;game_MCC1.c'],['../group__mike.html#ga356fa68055363ee50a26734ceac6683c',1,'Game_DrawMap():&#160;game_MCC1.c']]],
  ['game_5fover',['Game_Over',['../group__kevin.html#ga6086f566653d19a4ae0dd3fafda5d77e',1,'game_KGAME2.c']]],
  ['game_5frecttext',['Game_RectText',['../group__iryna.html#ga6ff307382e726339a47838b2cf5863ef',1,'Game_RectText(char c[], char x_min, char y_min, char x_max, char y_max):&#160;testgame.c'],['../group__iryna.html#ga6ff307382e726339a47838b2cf5863ef',1,'Game_RectText(char c[], char x_min, char y_min, char x_max, char y_max):&#160;testgame.c']]],
  ['gameinit',['GameInit',['../group__mike.html#gad185e894111e1e73541e4fbbd63d14ca',1,'GameInit():&#160;game_MCC1.c'],['../group__mike.html#gad185e894111e1e73541e4fbbd63d14ca',1,'GameInit():&#160;game_MCC1.c']]],
  ['gameover',['GameOver',['../group__pkma.html#gadb9a48f7b54346c4e6f2a2b0fdf1e00b',1,'GameOver(uint8_t win):&#160;game_PKMA.c'],['../group__pkma.html#gadb9a48f7b54346c4e6f2a2b0fdf1e00b',1,'GameOver(uint8_t win):&#160;game_PKMA.c'],['../testgame_8c.html#afc2d34d7b7c82d3005b50d58427665e6',1,'GameOver(void):&#160;testgame.c'],['../group__iryna.html#gafc2d34d7b7c82d3005b50d58427665e6',1,'GameOver(void):&#160;testgame.h']]],
  ['gameplay',['gamePlay',['../group__iryna.html#ga9a5a8d92d0e15bd78ec44371f7ac9a73',1,'gamePlay(void):&#160;testgame.c'],['../group__iryna.html#ga9a5a8d92d0e15bd78ec44371f7ac9a73',1,'gamePlay(void):&#160;testgame.c']]],
  ['gamereceiver',['GameReceiver',['../group__mike.html#gaf05ccd2276e10a327d2859456790d6b5',1,'GameReceiver(char c):&#160;game_MCC1.c'],['../group__mike.html#gaf05ccd2276e10a327d2859456790d6b5',1,'GameReceiver(char c):&#160;game_MCC1.c']]],
  ['generategrass',['GenerateGrass',['../group__pkma.html#ga17e408b3023fa0d61e3720081fa8ea80',1,'GenerateGrass(void):&#160;game_PKMA.c'],['../group__pkma.html#ga17e408b3023fa0d61e3720081fa8ea80',1,'GenerateGrass(void):&#160;game_PKMA.c']]],
  ['getmapchar',['getMapChar',['../map_8c.html#ac147fa5cca0c92f95825c5a4f448776a',1,'getMapChar(char x, char y):&#160;map.c'],['../map_8h.html#ac147fa5cca0c92f95825c5a4f448776a',1,'getMapChar(char x, char y):&#160;map.c']]],
  ['gravity',['Gravity',['../group__mike.html#ga2f875f373d72bd7dfa10e68dd1348426',1,'Gravity():&#160;game_MCC1.c'],['../group__mike.html#ga2f875f373d72bd7dfa10e68dd1348426',1,'Gravity():&#160;game_MCC1.c']]]
];
