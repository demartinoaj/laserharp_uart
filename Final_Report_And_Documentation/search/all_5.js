var searchData=
[
  ['fcpu',['FCPU',['../system_8h.html#a9d4ad5467cb8a39312513dfaaf4d950a',1,'system.h']]],
  ['fight',['Fight',['../group__iryna.html#gace7a4cce5c3064a97445698f87fcb5a6',1,'Fight(void):&#160;testgame.c'],['../group__iryna.html#gace7a4cce5c3064a97445698f87fcb5a6',1,'Fight(void):&#160;testgame.c']]],
  ['fightheight',['FIGHTHEIGHT',['../testgame_8c.html#a5bf58c1805b0bc66abf8584a50fbd560',1,'testgame.c']]],
  ['fighting',['fighting',['../testgame_8c.html#a9476bc47560bdd53c4186cc7def5020c',1,'testgame.c']]],
  ['fightmovedown',['FightMoveDown',['../group__iryna.html#gae2b0af7b9903ca9bc4a5fea04eff7bf5',1,'FightMoveDown(void):&#160;testgame.c'],['../group__iryna.html#gae2b0af7b9903ca9bc4a5fea04eff7bf5',1,'FightMoveDown(void):&#160;testgame.c']]],
  ['fightmoveleft',['FightMoveLeft',['../group__iryna.html#ga6c3bca41c8ab19a0c7eaeda0b38921fe',1,'FightMoveLeft(void):&#160;testgame.c'],['../group__iryna.html#ga6c3bca41c8ab19a0c7eaeda0b38921fe',1,'FightMoveLeft(void):&#160;testgame.c']]],
  ['fightmoveright',['FightMoveRight',['../group__iryna.html#ga14142cb56f1aa55358d2f1335b39878e',1,'FightMoveRight(void):&#160;testgame.c'],['../group__iryna.html#ga14142cb56f1aa55358d2f1335b39878e',1,'FightMoveRight(void):&#160;testgame.c']]],
  ['fightmoveup',['FightMoveUp',['../group__iryna.html#ga4f0086c2b06dcb01a9e4d9b3ff27091a',1,'FightMoveUp(void):&#160;testgame.c'],['../group__iryna.html#ga4f0086c2b06dcb01a9e4d9b3ff27091a',1,'FightMoveUp(void):&#160;testgame.c']]],
  ['fightwidth',['FIGHTWIDTH',['../testgame_8c.html#a01fb112d1219c74035921829d746ec08',1,'testgame.c']]],
  ['find_5fzombies_5fxy',['Find_Zombies_XY',['../group__kevin.html#ga1792a711bfd34fbbc480135de381374e',1,'game_KGAME2.c']]],
  ['finishbattle',['FinishBattle',['../group__pkma.html#gaa5ea68a41580f2cd6a442cc799b61cd0',1,'FinishBattle(void):&#160;game_PKMA.c'],['../group__pkma.html#gaa5ea68a41580f2cd6a442cc799b61cd0',1,'FinishBattle(void):&#160;game_PKMA.c']]],
  ['fire',['Fire',['../group__kevin.html#ga929461f7a929b79b81e60776553e50da',1,'game_KGAME2.c']]],
  ['fire_5fspeed',['FIRE_SPEED',['../group__kevin.html#ga96258174015421062e4aa5638839434a',1,'game_KGAME2.c']]],
  ['full_5fmap_5finit',['Full_Map_Init',['../group__kevin.html#ga4835f242417150cd7ce7818267bd5f9a',1,'game_KGAME2.c']]]
];
