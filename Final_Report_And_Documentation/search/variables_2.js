var searchData=
[
  ['c',['c',['../structplayer.html#a675cc0c622f13183af276915d72b5d71',1,'player::c()'],['../structtestgame__t.html#a21d121079aa2559c4c19b169429d77ee',1,'testgame_t::c()']]],
  ['context',['context',['../struct_m_c_c__menu__t.html#a4d8eff7e1904cc85d9b2eb9763b10378',1,'MCC_menu_t']]],
  ['counter',['counter',['../testgame_8c.html#a617a47c70795bcff659815ad0efd2266',1,'testgame.c']]],
  ['currentpoke',['Currentpoke',['../group__pkma.html#ga139cf00d53f71a68ecd5c76db22eb714',1,'game_PKMA.h']]],
  ['cursor',['cursor',['../struct_m_c_c__menu__t.html#a015222ef251e98a44ad2f47af3ab744e',1,'MCC_menu_t']]],
  ['cursor_5fx',['cursor_x',['../struct_m_c_c__menu__t.html#a8cd1c9412b494cd0ac6b7a9bdb54c53c',1,'MCC_menu_t']]],
  ['cursor_5fxmax',['cursor_xmax',['../struct_m_c_c__menu__t.html#a3dfdc64ab94a3d157def33c1ed2d6a77',1,'MCC_menu_t']]],
  ['cursor_5fxmin',['cursor_xmin',['../struct_m_c_c__menu__t.html#a7462c13cba7b20aebb7ce25034c2d3bc',1,'MCC_menu_t']]],
  ['cursor_5fy',['cursor_y',['../struct_m_c_c__menu__t.html#a13636a3ad742965747e05cdda9438cd9',1,'MCC_menu_t']]],
  ['cursor_5fymax',['cursor_ymax',['../struct_m_c_c__menu__t.html#ad52d6d165e4be4b1d5a7adf17b4e5ffb',1,'MCC_menu_t']]],
  ['cursor_5fymin',['cursor_ymin',['../struct_m_c_c__menu__t.html#ad67c1bb4da716a8933a0a35c15f06b8f',1,'MCC_menu_t']]]
];
