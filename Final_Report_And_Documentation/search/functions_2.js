var searchData=
[
  ['callback',['Callback',['../group__kevin.html#ga15d00a21d9ac761d6bb653bc472fa67b',1,'Callback(int argc, char *argv[]):&#160;game_KGAME2.c'],['../group__mike.html#gaba611bfb34e9b139311eec6c14242627',1,'Callback(int argc, char *argv[]):&#160;game_MCC1.c'],['../group__mike.html#gaba611bfb34e9b139311eec6c14242627',1,'Callback(int argc, char *argv[]):&#160;game_MCC1.c'],['../testgame_8c.html#aba611bfb34e9b139311eec6c14242627',1,'Callback(int argc, char *argv[]):&#160;testgame.c'],['../group__iryna.html#ga15d00a21d9ac761d6bb653bc472fa67b',1,'Callback(int argc, char *argv[]):&#160;game_MCC1.c']]],
  ['canpass',['canPass',['../map_8c.html#ae17da191e43a813bd663bf92aaafa943',1,'canPass(char xpos, char ypos):&#160;map.c'],['../map_8h.html#ae17da191e43a813bd663bf92aaafa943',1,'canPass(char xpos, char ypos):&#160;map.c']]],
  ['catch',['Catch',['../group__pkma.html#ga3cc44ad25da7296baeb8457b7e340796',1,'Catch():&#160;game_PKMA.c'],['../group__pkma.html#ga3cc44ad25da7296baeb8457b7e340796',1,'Catch(void):&#160;game_PKMA.c']]],
  ['change_5fdirection',['Change_Direction',['../group__kevin.html#ga9d8cf4a0e158bd17db9cd759b9048d32',1,'game_KGAME2.c']]],
  ['checkchar',['checkChar',['../group__iryna.html#gab1de28da5b65e1c1467a30d804a2c277',1,'checkChar(char x, char y):&#160;testgame.c'],['../group__iryna.html#gab1de28da5b65e1c1467a30d804a2c277',1,'checkChar(char x, char y):&#160;testgame.c']]],
  ['checkcollision',['CheckCollision',['../group__pkma.html#gaec19364aec48f08089468ae6b322f0f3',1,'CheckCollision(void):&#160;game_PKMA.c'],['../group__pkma.html#gaec19364aec48f08089468ae6b322f0f3',1,'CheckCollision(void):&#160;game_PKMA.c']]],
  ['checkcollisionenemy',['CheckCollisionEnemy',['../group__iryna.html#gabce749ccef7b1cbaaf72a83dfe987d16',1,'CheckCollisionEnemy(char_object_t *enemy):&#160;testgame.c'],['../group__iryna.html#gabce749ccef7b1cbaaf72a83dfe987d16',1,'CheckCollisionEnemy(char_object_t *enemy):&#160;testgame.c']]],
  ['cleartext',['ClearText',['../group__iryna.html#ga03df649551c0d030878ec40faf175cff',1,'ClearText(void):&#160;testgame.c'],['../group__iryna.html#ga03df649551c0d030878ec40faf175cff',1,'ClearText(void):&#160;testgame.c']]]
];
