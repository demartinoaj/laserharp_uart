var searchData=
[
  ['sendenemy',['SendEnemy',['../group__iryna.html#ga6a79ba4d79a02e44dcf7b88285d5ff17',1,'SendEnemy(void):&#160;testgame.c'],['../group__iryna.html#ga6a79ba4d79a02e44dcf7b88285d5ff17',1,'SendEnemy(void):&#160;testgame.c']]],
  ['sendmidioverbluetooth',['sendMIDIOverbluetooth',['../rocketlaserharp_8c.html#a41aecb828b948dc6659d2d8093c5849f',1,'rocketlaserharp.c']]],
  ['sendmidioveruart',['sendMIDIOverUart',['../rocketlaserharp_8c.html#a68fe633c69a6981ff9b571c8d5ab9901',1,'sendMIDIOverUart(uint8_t *ptr, uint8_t len):&#160;rocketlaserharp.c'],['../group__lsrhrp.html#ga68fe633c69a6981ff9b571c8d5ab9901',1,'sendMIDIOverUart(uint8_t *ptr, uint8_t len):&#160;rocketlaserharp.h']]],
  ['solvedown',['solveDOWN',['../group__mike.html#ga2a9f299c832ea672eaf0d2432d97fbb7',1,'solveDOWN(char level, char x, char y):&#160;game_MCC1.c'],['../group__mike.html#ga2a9f299c832ea672eaf0d2432d97fbb7',1,'solveDOWN(char level, char x, char y):&#160;game_MCC1.c']]],
  ['solveleft',['solveLEFT',['../group__mike.html#ga07afd8a31acec4d42876971b4a6b1044',1,'solveLEFT(char level, char x, char y):&#160;game_MCC1.c'],['../group__mike.html#ga07afd8a31acec4d42876971b4a6b1044',1,'solveLEFT(char level, char x, char y):&#160;game_MCC1.c']]],
  ['solveright',['solveRIGHT',['../group__mike.html#gaabe10cda5755559e364efe8418fffabc',1,'solveRIGHT(char level, char x, char y):&#160;game_MCC1.c'],['../group__mike.html#gaabe10cda5755559e364efe8418fffabc',1,'solveRIGHT(char level, char x, char y):&#160;game_MCC1.c']]],
  ['solveup',['solveUP',['../group__mike.html#gac86bc406f74c59d582092edd07c94b2e',1,'solveUP(char level, char x, char y):&#160;game_MCC1.c'],['../group__mike.html#gac86bc406f74c59d582092edd07c94b2e',1,'solveUP(char level, char x, char y):&#160;game_MCC1.c']]],
  ['success',['Success',['../group__mike.html#gafb09b9aa06a21365d271b57231b41f74',1,'Success():&#160;game_MCC1.c'],['../group__mike.html#gafb09b9aa06a21365d271b57231b41f74',1,'Success():&#160;game_MCC1.c']]],
  ['supereffect',['SuperEffect',['../group__pkma.html#ga68ee5cf0fb9f79865b4b83cd398101d2',1,'SuperEffect(char attack, char defend):&#160;game_PKMA.c'],['../group__pkma.html#ga68ee5cf0fb9f79865b4b83cd398101d2',1,'SuperEffect(char attack, char defend):&#160;game_PKMA.c']]]
];
