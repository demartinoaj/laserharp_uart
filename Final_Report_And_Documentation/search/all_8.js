var searchData=
[
  ['id',['id',['../struct_k__game__t.html#aed1e2c991ca84b1b6a4628ec47de48ab',1,'K_game_t::id()'],['../struct_m_c_c__game__t.html#a2b6ec362b5805f070dd326bf4f5a6831',1,'MCC_game_t::id()'],['../structplayer.html#a56f1d1bcb21f8f91a02b54297b0e1323',1,'player::id()'],['../structtestgame__t.html#a4f9f770c3676ddaf359349de8a8cdeca',1,'testgame_t::id()']]],
  ['initialize_5fplayers',['Initialize_Players',['../group__kevin.html#ga5eb9e9898fa47e7e66b35431ff43d47d',1,'game_KGAME2.c']]],
  ['interact',['Interact',['../group__iryna.html#ga430c396fa25c39c360071dbd3f5022af',1,'Interact(char x, char y):&#160;testgame.c'],['../group__iryna.html#ga430c396fa25c39c360071dbd3f5022af',1,'Interact(char x, char y):&#160;testgame.c']]],
  ['ionian',['ionian',['../group__lsrhrp.html#gga8e8d3da3ac3510ba6f5aa1a4c8c82eaea1d3bb61b7161668cafbc39fe07a43f29',1,'rocketlaserharp.h']]],
  ['isalive',['isAlive',['../testgame_8c.html#a61b3ad8880455304856024fe5886b2d4',1,'testgame.c']]]
];
