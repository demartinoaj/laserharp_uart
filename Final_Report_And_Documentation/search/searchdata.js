var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvwxyz",
  1: "bgkmptz",
  2: "gmrst",
  3: "abcdfghiklmoprstuvz",
  4: "abcdefghilmnoprstuvwxyz",
  5: "ms",
  6: "acdhilmoprtuw",
  7: "bfglmprstu",
  8: "dmrt",
  9: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

