var searchData=
[
  ['back',['Back',['../game___m_c_c1_8c.html#a809428666654e00b9ec50e5c120c4ace',1,'game_MCC1.c']]],
  ['battle_5fw',['Battle_w',['../group__pkma.html#ga0469ef8ec9e8140a344a8a679e76a182',1,'Battle_w():&#160;game_PKMA.c'],['../group__pkma.html#ga0469ef8ec9e8140a344a8a679e76a182',1,'Battle_w(void):&#160;game_PKMA.c']]],
  ['battleflg',['Battleflg',['../group__pkma.html#ga5917d15756310ad484ba8614136335d4',1,'game_PKMA.h']]],
  ['beam_5finit',['Beam_init',['../rocketlaserharp_8c.html#a64dee06652df866ceb6c2c8b9347c99e',1,'Beam_init():&#160;rocketlaserharp.c'],['../group__lsrhrp.html#ga64dee06652df866ceb6c2c8b9347c99e',1,'Beam_init():&#160;rocketlaserharp.h']]],
  ['bluefruit_5fchannel',['BLUEFRUIT_CHANNEL',['../system_8h.html#a096719a4d0243faf5542bd45fb3c9a7f',1,'system.h']]],
  ['bluetoothmode',['BLUETOOTHMODE',['../group__lsrhrp.html#ga857c65bf9841d7c7731c576a7bbe5b76',1,'rocketlaserharp.h']]],
  ['boost',['boost',['../struct_poke.html#a2221c4ac9b69c3bf05c230e35d85e0af',1,'Poke']]],
  ['bullet',['bullet',['../structbullet.html',1,'']]],
  ['bullet_5fat_5fxy',['Bullet_At_XY',['../group__kevin.html#gaad873d2fab09d11977f0e0e3f9d2a003',1,'game_KGAME2.c']]],
  ['bullet_5fchar',['BULLET_CHAR',['../group__kevin.html#ga8b55842a1b274039b467b3da250c8ee9',1,'game_KGAME2.c']]],
  ['bullet_5fkill_5fzombie',['Bullet_Kill_Zombie',['../group__kevin.html#ga8aa283cafcdadc129351a0a33b8262a9',1,'game_KGAME2.c']]],
  ['bullet_5flife',['BULLET_LIFE',['../group__kevin.html#ga7811b7b03233e9ecdd3eca3f08a2a905',1,'game_KGAME2.c']]],
  ['bullets',['bullets',['../group__kevin.html#ga63eda7eebdf707a417f2e6a149b57fa2',1,'game_KGAME2.c']]]
];
