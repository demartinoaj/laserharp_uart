/** @defgroup Rocket Laser Harp
 * File: rocketlaserharp.h
 * @author Team R.O.C.K.E.T: Andrew DeMartino, Mike McCaffrey, Iryna Topchiy, Kevin Tomkins
 * @{*/

#ifndef ROCKETLASERHARP_H_
/**MIDI channel to brodast notes over*/
#define MIDI_CHANNEL  1
/**number of beams in laser harp*/
#define NUM_BEAMS 8
/**define LOOPMODE to use a infinate while loop instead of the task scheduler*/
#define LOOPMODE
/**Period in ms that the task scheduler will check at*/
#define CHECK_PERIOD 50
/**define BLUETOOTHMODE to use BLE instead of UART to send midi*/
//#define BLUETOOTHMODE

/**
 * MIDI Callback that sends MIDI notes over UART
 * @param ptr pointer to data being sent
 * @param len Length of data*/
static void sendMIDIOverUart(uint8_t * ptr, uint8_t len);
/**
 * Initializes laser harp module*/
void LaserHarp_init(void);
/**
 * Checks lasers with ethier a while loop or task scheduler\n\r
 * Sends midi data over UART or Bluetooth*/
static void LaserCheck();
/**
 * Initializes input pins for detecting a beam break*/
static void Beam_init();
/////////////////////////////////////////////////
static uint8_t note_base = 0;
/** MIDI standard note*/
static uint8_t note = 0;
static uint8_t octave = 0;
static uint16_t note_status = 0;
/** MIDI standard velocity 0-127*/
static uint8_t vel=127;//no idea
enum Scale { ionian = 0, dorian, phrygian, lydian, mixolydian, aeolian, locrian, majorPenta, minorPenta, chromatic, whole, COUNT } scale;
static const uint8_t diatonic_base[] =  {0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24, 0};
static const uint8_t penta_base[] = {0, 3, 5, 7, 10, 12, 15, 17, 19, 22, 24, 27, 29, 31, 34, 0};
static uint8_t steps[15];
//////////////////////////////////////////////////
/** Array of input pin Buses, in order, for each pinbus used to detect a laser break*/
static const uint8_t laser_pinbus[NUM_BEAMS]={1,3,1,1,8,2,2,2};
/** Array of input pin bits, in order, for each pinbit used to detect a laser break\n
 * where a pin on the MSP430 is pinbus.pinbit ie pin 1.4*/
static uint8_t laser_pinbit[NUM_BEAMS]=      {4,7,3,5,2,2,3,4};
///////////////////////////////////////////////////
/**
 * @}
 */
#define ROCKETLASERHARP_H_



#endif /* ROCKETLASERHARP_H_ */
