/**
 * File: rocketlaserharp.h
 * @author Team R.O.C.K.E.T: Andrew DeMartino, Mike McCaffrey, Iryna Topchiy, Kevin Tomkins*/
#include "midi.h"
#include "system.h"
#include <stdint.h>
#include "midi_ble.h"
#include "bluefruit.h"

#ifndef ROCKETLASERHARP_H_
#error "rocketlaserharp.c requires rocketlaserharp.h to function; Include in system.h "
#endif

static void sendMIDIOverUart(uint8_t * ptr, uint8_t len) {
	UART_Write(UART_CHANNEL, ptr, len);
}

static void sendMIDIOverbluetooth(uint8_t * ptr, uint8_t len) {
	void bluefruit_GATTWriteChar(uint8_t id, char * data);
}

void LaserHarp_init(void){
	    Timing_Init();/* initializes task timing and UART if they haven't been already*/
	    Task_Init();
	    UART_Init(UART_CHANNEL);

#ifdef BLUETOOTH_MODE
	    MIDI_BLE_Init();
	    bluefruit_Init();
	    MIDI_Init(sendMIDIOverbluetooth);
#else
	    MIDI_Init(sendMIDIOverUart);/* intializes midi and provides a callback function to send notes*/
#endif
		EnableInterrupts();

		Beam_init();/** sets up beam pins using global arrays*/

			/** LEDS for vistual note feedback and easy laser re-alignment*/
			P1DIR |= BIT0;
			P4DIR |= BIT7;
			P1OUT &= ~BIT0;
			P4OUT &= ~BIT7;

			memcpy(steps, diatonic_base, NUM_BEAMS);/** IDK: ?*/
			octave = 5;
			note_base = 10;

			LaserCheck();/* runs laser check beam polling function*/
}

static void LaserCheck() {
	uint8_t i;
	uint8_t laser_hold;
	uint8_t laser_on;
#ifdef LOOPMODE
		while(1){/** infinite while loop mode, system pritorizes laser harp*/
#endif
			for(i=0;i<NUM_BEAMS;i++){/** Interate through all of the beams*/

				switch(laser_pinbus[i]){
					case 1:
						laser_on=(P1IN & laser_pinbit[i]);/**true if diode is reciving laser*/
						break;							/**false if beam is blocked*/
					case 2:
						laser_on=(P2IN & laser_pinbit[i]);
						break;
					case 3:
						laser_on=(P3IN & laser_pinbit[i]);
						break;
					case 4:
						laser_on=(P4IN & laser_pinbit[i]);
						break;
					case 5:
						laser_on=(P5IN & laser_pinbit[i]);
						break;
					case 6:
						laser_on=(P6IN & laser_pinbit[i]);
						break;
					case 7:
						laser_on=(P7IN & laser_pinbit[i]);
						break;
					case 8:
						laser_on=(P8IN & laser_pinbit[i]);
						break;
				}

				switch(i){
					case 0:
						laser_hold=(BIT0);/** assigns laser_hold to the desired bitmask to read from note status*/
						break;			/** i.e. if beam 0 is being held then note_status&Bit0 will be true*/
					case 1:
						laser_hold=(BIT1);
						break;
					case 2:
						laser_hold=(BIT2);
						break;
					case 3:
						laser_hold=(BIT3);
						break;
					case 4:
						laser_hold=(BIT4);
						break;
					case 5:
						laser_hold=(BIT5);
						break;
					case 6:
						laser_hold=(BIT6);
						break;
					case 7:
						laser_hold=(BIT7);
						break;
				}

				if (!(laser_on) && !(note_status & laser_hold)){/** checks if beam is broken and laser isn't being held*/
					note = note_base + octave * 12 + steps[i];/** calculations to derrive note from note base octave and*/
					note_status |= (laser_hold);			/** @TODO probally need better explaination*/
					//led
					P1OUT |= BIT0;/** blink the L.E.D*/
					MIDI_SendNote(MIDI_CHANNEL, note, vel);/** laser break has been detected, send midi*/
				}
				// check for released note
				else if ((laser_on) && (note_status & laser_hold)){/** checks if beam isn't brocken and laser is being held*/
					note = note_base + octave * 12 + steps[i];
					note_status &= ~(laser_hold);
					// led
						P1OUT &= ~BIT0;
						MIDI_SendNoteOff(MIDI_CHANNEL, note);/** laser release has been detected, send midi off*/
				}
			}
#ifdef LOOPMODE
			SystemTick();/** other modules may not run as well, but laser harp has minimal latency in this mode*/

		}
#else
		Task_Schedule(LaserCheck, 0, 0, CHECK_PERIOD);/** Laser Harp runs on tash Scheduler, longer delay between beam break and play*/
					/** other non-laser harp modules will run at normal speeds in this mode*/
#endif
}

static void Beam_init(){
	uint8_t i;
	for(i=0;i<NUM_BEAMS;i++){/**Iterate through all elements in pinbit array*/

		switch(laser_pinbit[i]){/**check the number that refers to the pin bit*/
			case 0:
				laser_pinbit[i]=BIT0;/**replace the pin bit number with BIT0 for masking*/
				break;
			case 1:
				laser_pinbit[i]=BIT1;/**replace the pin bit number with BIT1 for masking*/
				break;
			case 2:
				laser_pinbit[i]=BIT2;
				break;
			case 3:
				laser_pinbit[i]=BIT3;
				break;
			case 4:
				laser_pinbit[i]=BIT4;
				break;
			case 5:
				laser_pinbit[i]=BIT5;
				break;
			case 6:
				laser_pinbit[i]=BIT6;
				break;
			case 7:
				laser_pinbit[i]=BIT7;/**replace the pin bit number with BIT7 for masking*/
				break;
		}
	}
	for(i=0;i<NUM_BEAMS;i++){/**Iterate through all elements in pinbus array*/

		switch(laser_pinbus[i]){
			case 1:
				P1DIR &=~(laser_pinbit[i]);/**set pins defined by the two global pin arrays as inputs to detect lasers*/
				break;
			case 2:
				P2DIR &=~(laser_pinbit[i]);
				break;
			case 3:
				P3DIR &=~(laser_pinbit[i]);
				break;
			case 4:
				P4DIR &=~(laser_pinbit[i]);
				break;
			case 5:
				P5DIR &=~(laser_pinbit[i]);
				break;
			case 6:
				P6DIR &=~(laser_pinbit[i]);
				break;
			case 7:
				P7DIR &=~(laser_pinbit[i]);
				break;
			case 8:
				P8DIR &=~(laser_pinbit[i]);
				break;
		}

	}
}
