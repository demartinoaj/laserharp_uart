/*
 * game_PKMA.c
 *
 *  Created on: Mar 19, 2015
 *      Author: Andrew DeMartino
 */
////////////////////////////

#include "system.h"
#include "random_int.h"
#include "strings.h"
#include<string.h>
#include "game_PKMA.h"

void PKMA_Init(void) {
    /** Register the module with the game system and give it the name "PKMA"*/
    pokegame.id = Game_Register("PKMA", "Pokemon Ascii Version", TitleScreen, Help);
}

void Help(void) { Game_Printf("It's pokemon. Kill monsters; Get lvls\r\n"); }


void TitleScreen(void) {
    Game_Printf("\fPOKEMON ASCII VERSION\r\n"
            "WASD to move\r\n"
            "Game will begin in 5 seconds.\n\r");
    /** Use the Task Management Module to schedule the function Play() to run
     5000ms from now and set period to 0 so it never runs again*/
    Task_Schedule(Play, 0, 5000, 0);
}

void Play(void) {
    volatile uint8_t i;
    /** determine starter*/
    Pokelookup(STARTER, START_LVL);
    /** set first pokemon team member equal to starter*/
    PlayerTeam[0]=tempmon;
    /** set other two team members to dead*/
    PlayerTeam[1].status='0';
    PlayerTeam[2].status='0';
    PlayerTeam[1].hp=0;
    PlayerTeam[2].hp=0;
    tempmon.status='0';
    /** Reset globals*/
    Battleflg=0;
    Currentpoke=0;
    Tile2flg=0;
    Trainkill=0;

    /** clear the screen*/
    Game_ClearScreen();
    /** draw a box around our map*/
    Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT);
    Game_SetColor(red);
    /** initialize game variables*/
    pokegame.x = (MAP_WIDTH/2);
    pokegame.y= MAP_HEIGHT-1;
    pokegame.c = CHARACTER;
    /* draw the player at the bottom center of the map*/
    Game_CharXY(pokegame.c, MAP_WIDTH/2, MAP_HEIGHT-1);
    Game_SetColor(white);
    Game_RegisterPlayer1Receiver(Receiver);
    /* hide cursor*/
    Game_HideCursor();
    /** generate the first map*/
    GenerateGrass();
}

void Receiver(char c) {
	int16_t loss;
	uint8_t move;
	uint8_t multi;

	if(!Battleflg){/** No battle flag: uses wasd to move*/
		switch (c) {
			case 'a':
				MoveLeft();
				CheckCollision();
				break;
			case 'd':
				MoveRight();
				CheckCollision();
				break;
			case 'w':
				MoveUp();
				CheckCollision();
				break;
			case 's':
				MoveDown();
				CheckCollision();
				break;
			case '\r':
				GameOver(0);
				break;
			default:/** Switch statement is to avoid input error; best if it only works if it transmits correctly*/
				break;
		}
	}
	else{
		/** battle flag high, 1-4 to use a move*/
		switch (c) {

			case '1':/** basic move*/
				move=1;
				loss=(PlayerTeam[Currentpoke].attack-tempmon.res);/*the attk of the player poke - the res of the other*/
				if(loss>0){
					tempmon.hp=tempmon.hp-loss;/** if loss is not less than 1*/
				}else{
					tempmon.hp--;
				}
				Moveprint(PlayerTeam[Currentpoke].name, move);/** print the currrent pokemons move set/hp*/
				printhp();
				/** give 1 second to read move*/
				Task_Schedule(FinishBattle, 0, 1000, 0);
				break;

			case '2':/** super effective move*/
				multi=SuperEffect(PlayerTeam[Currentpoke].type, tempmon.type);/** find the mutiplier*/
				move=2;
				loss=((PlayerTeam[Currentpoke].attack)+2*multi)-tempmon.res;
				if(multi>0 && loss>0){
					tempmon.hp=tempmon.hp-loss;
				}else{
					tempmon.hp=tempmon.hp;/** move has no effect*/
				}
				Moveprint(PlayerTeam[Currentpoke].name, move);
				printhp();
				Task_Schedule(FinishBattle, 0, 1000, 0);
				break;

			case '3':/** stat boost move*/
				if(PlayerTeam[Currentpoke].boost=='A'){/** if the pokemons move boosts attack*/
					PlayerTeam[Currentpoke].attack+=1;
				}else{
					PlayerTeam[Currentpoke].res+=1;/** if the pokemons move boosts resistance*/
				}
				move=3;
				Moveprint(PlayerTeam[Currentpoke].name, move);/** print x pokemon used y attack*/
				printhp();
				Task_Schedule(FinishBattle, 0, 500, 0);
				break;

			case '4':
				if(Tile2flg){/** make sure its not a trainer battle*/
					/*NO*/
					Game_CharXY('Y', 1, MAP_HEIGHT-1);
					Game_Printf("ou can't capture a trainers pokemon!");
				}else{
				Catch();/**run catch function*/
				}
				break;

			case '\r':
				GameOver(0);
				break;
			default:
				break;
		}
	}
}

void GenerateGrass(void) {
	char_object_t * grass = 0;
    char x;
    char y;
    volatile uint8_t i;
    /** set the grass to green*/
    Game_SetColor(green);
    /** create and print MAX_GRASS_T1 grass tiles*/
	for(i = 0; i < MAX_GRASS_T1; i++){
		grass = &Grass1[i];
		/** get random  x location*/
		x = random_int(3, MAP_WIDTH-3);
		/** get random y location*/
		y = random_int(3, MAP_HEIGHT-3);
		grass->status = 1;
		grass->x = x;
		grass->c = 134;
		grass->y = y;
		Game_CharXY(134, x, y);
    }
	Game_SetColor(white);
}

void MoveRight(void) {
    /** make sure the player can move right*/
	Game_SetColor(red);
    if (pokegame.x < MAP_WIDTH - 1) {
        /** erase player */
        Game_CharXY(' ', pokegame.x, pokegame.y);
        pokegame.x++;
        /*redraw player*/
        Game_CharXY(pokegame.c, pokegame.x, pokegame.y);
    }
    Game_SetColor(white);
}

void MoveLeft(void) {
	Game_SetColor(red);
	/* make sure the player can move left*/
    if (pokegame.x > 1) {
    	/** erase player */
        Game_CharXY(' ', pokegame.x, pokegame.y);
        pokegame.x=pokegame.x-1;
        /*redraw player*/
        Game_CharXY(pokegame.c, pokegame.x, pokegame.y);
    }
    Game_SetColor(white);
}

void MoveUp(void) {
	/** make sure the player can move up*/
	Game_SetColor(red);
    if (pokegame.y > 1) {
    	/** erase player */
        Game_CharXY(' ', pokegame.x, pokegame.y);
        /**move up*/
        pokegame.y=pokegame.y-1;
        /*redraw player*/
        Game_CharXY(pokegame.c, pokegame.x, pokegame.y);
    }
    Game_SetColor(white);
    /** player reached end of map*/
    if (pokegame.y == 1 && ! Tile2flg){/* if on first, draw second map*/
    	Tile2();
    }else if (pokegame.y == 1 && Tile2flg){/* if on second, win game*/
    	GameOver(1);
    }
}

void MoveDown(void) {
	Game_SetColor(red);
    /** first make sure the player ca move down */
    if (pokegame.y <MAP_HEIGHT - 1) {
    	/** erase player */
        Game_CharXY(' ', pokegame.x, pokegame.y);
        /**move down*/
        pokegame.y++;
        /*redraw player*/
        Game_CharXY(pokegame.c, pokegame.x, pokegame.y);
    }
    Game_SetColor(white);
}

void CheckCollision(void) {
    volatile uint8_t i;
    char_object_t * grass=0;
    for(i = 0; i < MAX_GRASS_T1; i++) {
        grass = &Grass1[i];/** set pointer to point to the GRASS1 object for total number of the grass*/
        if(grass->status) {/** make sure the grass is alive*/
            if (pokegame.x == grass->x && pokegame.y == grass->y) {
            	/** colision deteced, enter battle*/
            	grass->status=0;/**kill grass*/
            	Battleflg=1;/** raise battle flag and enter battle*/
                Battle_w();
            }
        }
    }
    if(Tile2flg){/** same as before, but with trainers*/
    	for(i = 0; i < 3; i++){
    		grass=&Trainers[i];/** grass varrible was reused*/
    		if(grass->status){
    			if ( pokegame.y == grass->y) {
                	grass->status=0;
                	Battleflg=1;/** raise battle flag and enter battle*/
                    Battle_w();
    			}
    		}
    	}
    }
}

void Battle_w(){
	if(!Battleflg){
		GameOver(0);
		return;
	}
	Game_Bell();/** play theme*/
	volatile uint8_t i=0;
	volatile uint8_t loss=0;
	//Battleflg=1;/** set battle flag high*/
	Game_ClearScreen();
	Game_DrawRect(0, MAP_HEIGHT-5, MAP_WIDTH, MAP_HEIGHT);/** set battle draw move box*/
	volatile uint8_t y;

	if(tempmon.status=='0'){
	y = random_int(1, PKM_SPECIES);
	Pokelookup(y, Trainkill+1);/** level increase for each trainer beaten (always 1 for wild)*/
	}
	printhp();

	for(i=0; i<4; i++){/** print both pokemon sprites*/
		Game_CharXY(PlayerTeam[Currentpoke].name, 1, MAP_HEIGHT-13+i*2);
		Game_CharXY(PlayerTeam[Currentpoke].name, 3, MAP_HEIGHT-13+i*2);
		Game_CharXY(PlayerTeam[Currentpoke].name, 5, MAP_HEIGHT-13+i*2);
		Game_CharXY(PlayerTeam[Currentpoke].name, 7, MAP_HEIGHT-13+i*2);

		Game_CharXY(tempmon.name, MAP_WIDTH, i*2);
		Game_CharXY(tempmon.name, MAP_WIDTH-2, i*2);
		Game_CharXY(tempmon.name, MAP_WIDTH-4, i*2);
		Game_CharXY(tempmon.name, MAP_WIDTH-6, i*2);
	}

	Moveprint(PlayerTeam[Currentpoke].name, 0);/** print players moves*/
}

void GameOver(uint8_t win) {
	Game_UnregisterPlayer1Receiver(Receiver);/** unregister the receiver used to run the game */
	Game_ClearScreen();
    /** set cursor below bottom of map*/
    Game_CharXY('\r', 0, MAP_HEIGHT + 1);
    Game_Printf("Game Over!\n");
    if(win==1){/** if player won*/
    	Game_Printf("\r You Win!\n\r");
    }
    /** show cursor*/
    Game_ShowCursor();
}

void Pokelookup(uint8_t new, uint8_t lvl){
	tempmon.status='1';/** enable pokemon*/
	tempmon.attack=lvl;/** set stats,determined by level*/
	tempmon.res=lvl;
	tempmon.lvl=lvl;
	tempmon.hp=lvl*5+5;
	switch(new){/** based on new pokemon number determine the rest of stats*/
		case 0:
			tempmon.name=216;/** capacitor symbol*/
			tempmon.type='S';/** pokemon type: symbol*/
			tempmon.boost='R';/** move 3 increase resistance*/
		break;

		case 1:
			tempmon.name='*';/** charcter that prints durring battle*/
			tempmon.type='M';
			tempmon.boost='A';/** move 3 increases attack*/
			break;
		case 2:
			tempmon.name='{';
			tempmon.type='P';
			tempmon.boost='A';/** move 3 increases attack*/
			break;
	}
}
static void Moveprint(char name, uint8_t move){

	if(move ==0){/** print moveset*/
		switch(name){/** switch based on pokemon species*/
			case 216:
				Game_CharXY('1', 1, MAP_HEIGHT-4);/** print move 1 in box*/
				Game_Printf(". Symbolize");

				Game_CharXY('2', 15, MAP_HEIGHT-4);/** print move 2 in box*/
				Game_Printf(". Discharge");

				Game_CharXY('3', 1, MAP_HEIGHT-3);/** print move 3 in box*/
				Game_Printf(". Ohm's Law");
			break;

			case '*':
				Game_CharXY('1', 1, MAP_HEIGHT-4);
				Game_Printf(". Calculate");
				Game_CharXY('2', 21, MAP_HEIGHT-4);
				Game_Printf(". Convolve");
				Game_CharXY('3', 1, MAP_HEIGHT-3);
				Game_Printf(". Muliply");
				break;

			case '{':
				Game_CharXY('1', 1, MAP_HEIGHT-4);
				Game_Printf(". Puncture");
				Game_CharXY('2', 21, MAP_HEIGHT-4);
				Game_Printf(". Organize");
				Game_CharXY('3', 1, MAP_HEIGHT-3);
				Game_Printf(". Transform");
				break;
		}
		Game_CharXY('4', 15, MAP_HEIGHT-3);
		Game_Printf(". Throw Pokeball");/** move four is throw pokeball for all*/
		return;
	}
	else{

		Game_CharXY(' ', 1, MAP_HEIGHT-1);
		Game_Printf("                                   ");/** clear last move*/
		Game_CharXY(name, 1, MAP_HEIGHT-1);//** x pokemon*/
		Game_Printf(" used ");//** used */
		switch(name){

			case 216:
				switch(move){
				case 1:
					Game_Printf("Symbolize");//** corresponding move*/
				break;
				case 2:
					Game_Printf("Discharge");
				break;
				case 3:
					Game_Printf("Ohm's Law");
				break;}
			break;

			case '*':
				switch(move){
				case 1:
					Game_Printf("Calculate");
				break;
				case 2:
					Game_Printf("Convolve");
				break;
				case 3:
					Game_Printf("Muliply");
				break;}
			break;

			case '{':
				switch(move){
				case 1:
					Game_Printf("Puncture");
				break;
				case 2:
					Game_Printf("Organize");
				break;
				case 3:
					Game_Printf("Transform");
				break;}
			break;
		}
	}
}
void Catch(){
	volatile uint16_t catchrate;
	volatile uint8_t rnum;
	volatile uint8_t i;

	catchrate=(tempmon.hp-RATE_MOD)/2+1;/** catch rate based on HP i.e. 50% a 1*/
	rnum= random_int(0, catchrate);/** 1 in catchrate chance to catch */
	Game_CharXY(' ', 1, MAP_HEIGHT-1);// clear pokemon
	Game_Printf("                              ");//clear last move

	Game_CharXY(tempmon.name, 1, MAP_HEIGHT-1);

	if	(rnum==0){/** successfully caught*/
		/*Game_Printf("                              ");
		Game_CharXY(tempmon.name, 1, MAP_HEIGHT-1);
		Game_Printf(" WAS CAPTURED");*/

	    for(i = 0; i < 3; i++) {

	        if(PlayerTeam[i].status=='0') {/** pokemon is unused, not dead*/
	    		Game_CharXY(tempmon.name, 1, MAP_HEIGHT-1);
	    		Game_Printf(" WAS CAPTURED");//clear last move
	        	tempmon.status='1';
	        	PlayerTeam[i]=tempmon;
	        	PlayerTeam[i].status='1';/** set the temp pokemon onto the play team if room is avaialble*/
	        	tempmon.hp=0;
	        	Task_Schedule(FinishBattle, 0, 2000, 0);
	        	return;
	        }
	        if(PlayerTeam[2].status=='1'){/**PC WIP*/
	    		Game_CharXY(' ', 1, MAP_HEIGHT-1);
	    		Game_Printf("                              ");//clear last move
	    		Game_CharXY('P', 1, MAP_HEIGHT-1);
	    		Game_Printf("okemon caught and sent to PC");
	    		tempmon.hp=0;
	    		Task_Schedule(FinishBattle, 0, 2000, 0);
	    		return;
	        }
	    }
	}
		Game_Printf("                              ");
		Game_CharXY(tempmon.name, 1, MAP_HEIGHT-1);
		Game_Printf(" broke free");
		Task_Schedule(FinishBattle, 0, 1000, 0);
		//FinishBattle();
}
void printhp(){
	//erase previous
	Game_CharXY(' ', 9, MAP_HEIGHT-13);/** clear HP*/
	Game_Printf("     ",PlayerTeam[Currentpoke].hp);

	Game_CharXY(' ', MAP_WIDTH-13, 0);
	Game_Printf("     ",PlayerTeam[Currentpoke].hp);

	//print HP
	Game_CharXY('H', 9, MAP_HEIGHT-13);/** set HP*/
	Game_Printf("P: %d",PlayerTeam[Currentpoke].hp);

	Game_CharXY('H', MAP_WIDTH-13, 0);
	Game_Printf("P: %d",tempmon.hp);
	//
}
uint8_t SuperEffect(char attack, char defend){
	/**M>S, S>P, P>M*/
	/** move do not effect pokeon of the same type*/
	switch(attack){
	case 'M':/** attack pokemon is type math*/
		if(defend=='S'){/** defend pokeon is type symbol*/
			return 1;/**move is super effective*/
		}
		else{/** defend pokemon is type punctuation or math*/
			return 0;/** move has no effect*/
		}
	break;

	case 'S':/** attack pokemon is type symbol*/
		if(defend=='P'){/** defend pokemon is type punctuation*/
			return 1;
		}
		else{/** defend pokemon is type math or symbol*/
			return 0;
		}
	break;

	case 'P':/** attack pokemon is type punctuation*/
		if(defend=='M'){
			return 1;
		}
		else{/** defend pokemon is type symbol or punctuation*/
			return 0;
		}
	break;
	}
	return 255;/** ERROR*/
}
void FinishBattle(void){
	int16_t loss;
	uint8_t multi;

	if(tempmon.hp<1){
		tempmon.status='0';
		if(Tile2flg){
			Trainkill=Trainkill+1;
		}
		Task_Schedule(RegenGrass, 0, 1000, 0);
		return;
	}
	uint8_t move;
	move= random_int(1, 3);/** enemy pokemon "A.I"*/
	Moveprint( tempmon.name, move);
	switch (move) {/*same as abvove but damage is done to player*/
		case 1:
			move=1;
			loss=(tempmon.attack-PlayerTeam[Currentpoke].res);
			if(loss>0){
				PlayerTeam[Currentpoke].hp=PlayerTeam[Currentpoke].hp-loss;
			}else{
				PlayerTeam[Currentpoke].hp--;
			}
			break;
		case 2:
			multi=SuperEffect(tempmon.type, PlayerTeam[Currentpoke].type);
			move=2;
			loss=((tempmon.attack)+2*multi)-PlayerTeam[Currentpoke].res;//the attk of the poke - the res of the other
			if(multi>0 && loss>0){
				PlayerTeam[Currentpoke].hp=PlayerTeam[Currentpoke].hp-loss;
			}else{
				PlayerTeam[Currentpoke].hp=PlayerTeam[Currentpoke].hp;
			}
			break;

		case 3:
			if(tempmon.boost=='A'){
				tempmon.attack++;
			}else{
				tempmon.res++;
			}
			break;
	}
	printhp();
	if(PlayerTeam[Currentpoke].hp <1){/** if the player dies switch pokemon*/
		Currentpoke++;

		if(PlayerTeam[Currentpoke].status=='0'){/** player is out of pokemon*/
			Task_Schedule(GameOver, 0, 500, 0);
			Battleflg=0;
			return;
		}
		/** switch pokemon*/
		tempmon.attack=tempmon.lvl;
		tempmon.res=tempmon.lvl;
		Battle_w();
	}

}

void RegenGrass(){
	Regenstats();/**so that pokemon are healed for next battle*/
	Battleflg=0;/** the battle has ended reset flag, retun to map*/
    Game_ClearScreen();
    Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT);

    Game_SetColor(red);
    Game_CharXY(pokegame.c, pokegame.x, pokegame.y);
    Game_SetColor(green);
	uint8_t y;
	uint8_t x;
	uint8_t i;
	char_object_t * grass = 0;
	if(!Tile2flg){
		for(i = 0; i < MAX_GRASS_T1; i++){/** interates through grass array*/
			grass = &Grass1[i];
			if (grass->status == 1){/** prints if grass is active*/
				x= grass->x ;
				y= grass->y;
				Game_CharXY(134, x, y);
			}
		}
	}else{/** on map 2 not grass map*/
	Game_SetColor(green);//green boarder
	    Game_ClearScreen();
	    // draw a box around our map
	    Game_DrawRect(0, 0, MAP_WIDTH+1, MAP_HEIGHT);
	    // initialize game variables

	    Game_SetColor(red);//red trainers
	    Game_CharXY(pokegame.c, pokegame.x, pokegame.y);
	    Game_SetColor(yellow);
		for(i = 0; i < 3; i++){
			grass = &Trainers[i];/** trainers do not disapper if you beat them*/
			x= grass->x ;
			y= grass->y;
			Game_CharXY(grass->c, x, y);
		}
		Game_SetColor(white);

	}
}

void Tile2(){
	Tile2flg=1;/** same as above but with trainers insead of grass*/
	char_object_t * grass = 0;
	uint8_t i;
	for(i = 0; i < MAX_GRASS_T1; i++){/** deactivate all remaining grass*/
		grass = &Grass1[i];
		grass->status =0;
	}

	Game_ClearScreen();
	Game_SetColor(green);
	Game_DrawRect(0, 0, MAP_WIDTH+1, MAP_HEIGHT);

	Game_SetColor(red);
    pokegame.x = (MAP_WIDTH/2);
    pokegame.y= MAP_HEIGHT-1;
	Game_CharXY(pokegame.c, pokegame.x, pokegame.y);
	Game_SetColor(yellow);

	for(i=0;i<2;i++){
	Trainers[i].c='T';
	Trainers[i].y=random_int(3+10*i, 12+20*i);
	Trainers[i].x=MAP_WIDTH;
	Trainers[i].status='1';
	}
	Trainers[2].c='G';
	Trainers[2].y=2;
	Trainers[2].x=MAP_WIDTH;
	Trainers[i].status='1';

	for(i=0;i<3;i++){
		Game_CharXY(Trainers[i].c, Trainers[i].x, Trainers[i].y);
	}
	Game_SetColor(white);
}

void Regenstats(){
	uint8_t i;/** reset all pokemon stats, healing injured */
	uint8_t lvl=0;
	Currentpoke=0;
	for(i=0; i<3; i++){
		lvl=PlayerTeam[i].lvl;
#ifndef HARDMODE
		if(Trainkill>0){/** So that the starter does not lose lvls when START_LVL set to 2+*/
			lvl+=(Trainkill);
			PlayerTeam[i].lvl=lvl;
		}
#endif
		if(PlayerTeam[i].status=='1'){
			PlayerTeam[i].attack=lvl;
			PlayerTeam[i].res=lvl;
			PlayerTeam[i].hp=lvl*5+5;
		}
	}
}
