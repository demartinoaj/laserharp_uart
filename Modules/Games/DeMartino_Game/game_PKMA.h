/*
 * game_PKMA.h
 *
 *  Created on: May 9, 2016
 *      Author: Andrew
 */

#ifndef GAME_PKMA_H_
////EDITABLE
/** Changes the width of the map*/
#define MAP_WIDTH 40
/** Changes the height of the map */
#define MAP_HEIGHT 40
/** Changes the intial level of the starting pokemon and therefore game difficulty/n
 *  1 for hard 2 for normal 3 for easy */
#define START_LVL 1
/** Changes the amount of glass tiles one the first map */
#define MAX_GRASS_T1 10
/** Changes the type of the starter pokemon */
#define STARTER 0
/** Changes the character used to display the player*/
#define CHARACTER '@'
/** Changes the capture difficulty posive number to decrease, negative to increase */
#define RATE_MOD 0
/** The above macros may be changed to effect gameplay, the below may NOT */
//#define HARDMODE
/** if hardmode is defined your pokemon no longer level up, but trainers and gym leads do*/
///////NON EDITABLE
/** Total number of PKMN species - the starter
 * should not be changed unlessmore pokemon are added */
#define PKM_SPECIES 2
/** color globals */
static enum term_color green=ForegroundGreen;
static enum term_color white=ForegroundWhite;
static enum term_color red=ForegroundRed;
 static enum term_color yellow=ForegroundYellow;

/** Player Characer structure holds player data
 * @param x
 * @param y
 * @param c
 * @param id */
struct player {
    char x; ///< x coordinate of player
    char y;
    char c; ///< character of player
    uint8_t id; ///< ID of game=
};
/** PKMN structure for creating a pokemon object
 * @param type math, symbol, or punctuation (determines effectiveness)
 * @param name char to be printed durring battle
 * @param boost which stat the third move increases
 * @param status whether the pokeon is fainted
 * @param hp total Hit Points of pokemon
 * @param lvl level of pokemon
 * @param attack pokemon's attack stat
 * @param res pokemon's resistance to attack stat */
struct Poke {
	char type;
	char name;
	char boost;
	char status;
	int16_t hp;
	uint8_t lvl;
	uint8_t attack;
	uint8_t res;
};
/** Array of grass tiles */
char_object_t Grass1[MAX_GRASS_T1];
/** Array of trainers*/
char_object_t Trainers[3];

static struct player pokegame;
static struct Poke PlayerTeam[3];
static struct Poke tempmon;//result of poke lookup


/** Flag high if in battle */
static uint8_t Battleflg=0;
/** Current pokemon being used in battle */
static uint8_t Currentpoke=0;
/** Flag high if the secod map has been reached */
static uint8_t Tile2flg=0;
/** number of trainers killed */
static uint8_t Trainkill=0;
//////////////

/** UART reciever, takes data from the serial terminal and translates it into movement\n
 * If battle flag is high accepts battle commmands and preforms battle caclulations for the players turn
 */


static void Receiver(char c);
/** prints the moves of a pokemon to the screen
 * @parm name the name of the pokemon
 * @parm which of its 3 moves it used*/
static void Moveprint(char name, uint8_t move);
/** creates a pokemon object and determines moveset, type and stats\n
 * changes the global variable for ethier a new wild pokemon, or the trainers starter
 * @new the number of the pokemon being created
 * @lvl the level of pokemon being creates
 */
static void Pokelookup(uint8_t new, uint8_t lvl);
/** determines whether or not a pokemon was caught based on random numbers\n
* adds a pokemon the global variable playerteam if one was caught
 */
static void Catch(void);
/** determines the super effective muliplier\n
*M>S, S>P, P>M\n
* moves do not effect pokeon of the same type
* @param attack type of the attacking pokemon
* @param defend type of the defending pokeon
 */
static uint8_t SuperEffect(char attack, char defend);
/** Re-prints the hp of both pokemon in battle, after change */
static void printhp(void);
/** Resets the stats of all of the trainers pokeon after battle */
static void Regenstats(void);

/** Shows the title screen of the game for 5 seconds, runs Play() */
static void TitleScreen(void);
/** Intializes game varribles ans starts game */
static void Play(void);
/** Displays useful help text */
static void Help(void);

/** Moves character up on screen and onto second map if top is reached */
static void MoveUp(void);
/** Moves character down on screen*/
static void MoveDown(void);
/** Moves character right on screen*/
static void MoveRight(void);
/** Moves character left on screen */
static void MoveLeft(void);
/** Generates grass on first map, draws the first map */
static void GenerateGrass(void);
/** Re-draws grass and first map after battle screen */
static void RegenGrass(void);
/** Draws the second map, generates trainers to fight */
static void Tile2(void);

/** Initizes Pokemon battle, creates opponent pokemon, draws battle screen and sets battle flag high */
static void Battle_w(void);
/** Runs the oppents turn in a battle, end battle if the player loses */
static void FinishBattle(void);
/** Ends the game
 * @param win set high if the player won the game*/
static void GameOver(uint8_t win);
/** checks collesion between player and object
*/
static void CheckCollision(void);

#define GAME_PKMA_H_



#endif /* GAME_PKMA_H_ */
