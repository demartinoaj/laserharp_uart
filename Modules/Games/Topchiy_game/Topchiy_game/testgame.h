/** @file
 * Header file for the testgame.c class. The testgame class creates an ASCII game to be played on the terminal over UART where the user walks around on a map and interacts with objects on the map.
 * The user is able to interact with non-playable characters and go into a "fight" mode where the user has to dodge a predefined amount of enemies.
 * @author Iryna Topchiy
 */

#ifndef TESTGAME_H_
#define TESTGAME_H_

#include "system.h"
#include "random_int.h"
#include "strings.h"
#include "game.h"
#include "map.h"

/**Structure for the game containing the user's character, the x and y coordinate, the status, and the id of the game.
 */
struct testgame_t {
	char c;//user character
	char x;//x coordninate
	char y;//y coordninate
	char status;
	uint8_t id; //id of game
};

/** Initializes and registers the game.
 */
void testgame_Init(void);

/**Help menu.
 */
void Help(void);

/**Callback for the reset function.
 * @param argc - integer defining how many items to look for in argv[]
 * @param argv[] - pointer array of characters for the callback
 */
void Callback(int argc, char * argv[]);
/** This prints info before the game loads. The information printed displays the controls to the game. The task scheduler schedules for the game to start in 3 seconds after it is called.
 */
void PrePlay(void);

/** This function is called to play the game. The screen is cleared and a rectangle is drawn then the map is filled in.
 * The user is placed in the middle of the map and the receiver for the player is registered.
 *
 */
void Play(void);

/**Calls for the game to go back into the playing mode after at least 1 fight has been played. It clears the skin and redraws the map.
 * The user is put in the last position they were in in "fight" mode.
 */
void gamePlay(void);

/** Takes a char input, c. The input is from the terminal and is interpetted with this method as an action.
 * The actions that can be taken are move in all four directions, interect with the objects on the map, clear text in the textbox, and end the game.
 * For the movement, different functions are called based upon what game mode the user is in.
 * 'W','A','S',and 'D' in upper and lower case forms are used to walk. 'L' or 'l' is used to interact with objects around the user. ' ' (space) is used to clear the textbox.
 * Enter ends the game by calling GameOver().
 * @params c - character received over UART
 */
void Receiver(char c);

/** Moves the player to the right on the map when the player is in "play" mode.
 */
void MoveRight(void);

/** Moves the player to the left on the map when the player is in "play" mode.
 */
void MoveLeft(void);
/**Moves the player down on the map when the player is in "play" mode.
 */
void MoveDown(void);

/** Moves the player up on the map when the player is in "play" mode.
 */
void MoveUp(void);

/** Moves the player right when in the "fight" mode of the game.
 */
void FightMoveRight(void);
/**Moves the player left when in the "fight" mode of the game.
 */
void FightMoveLeft(void);

/**Moves the player down when in the "fight" mode of the game.
 */
void FightMoveDown(void);
/** Moves the player up when in the "fight" mode of the game.
 */
void FightMoveUp(void);
/** Moves the enemy down in "fight" mode and checks for a collision between the enemy and user.
 * @param * o - char object pointer to the enemy
 */
void MoveDownEnemy(char_object_t * o);

/**Moves the enemy up in "fight" mode and checks for a collision between the enemy and user.
 * @param * o - char object pointer to the enemy
 */
void MoveUpEnemy(char_object_t * o);

/**Moves the enemy right in "fight" mode and checks for a collision between the enemy and user.
 * @param * o - char object pointer to the enemy
 */
void MoveRightEnemy(char_object_t * o);

/**Moves the enemy left in "fight" mode and checks for a collision between the enemy and user.
 * @param * o - char object pointer to the enemy
 */
void MoveLeftEnemy(char_object_t * o);
/**Send an enemy during fight mode. The direction the enemy can come from is from the left, right, top or bottom or any combination of non-opposite sides, such as right up.
 * 8 different cases are used to decide what direction the enemies come from: left, bottom, right, top, and the combinations of each non-opposite pairs.
 * From each case, 5 enemies would be sent. After the 5 were sent, the next case is entered. The cycling through cases is done in a circular fashion.
 * Tasks are scheduled for each enemy to be sent and an enemy would be sent if less than max amount of enemies were sent during the fight.
 */
void SendEnemy(void);

/**This method deletes an enemy by setting it's status to 0 which deletes the enemy.
 * @params * o - char object pointer to the enemy
 */
void DeleteEnemy(char_object_t * o);

/**Checks if an enemy collided with the user. If the enemy collides with the user, the user is not alive, the enemy is deleted, and the game is over.
 * A '%' char is placed where the user died.
 * @param enemy - char object pointer to the enemy
 */
void CheckCollisionEnemy(char_object_t * enemy);

/**Draws test within a given area. In the game, the text is placed within the textbox below the map. Uses nested for statements to print the character array within the input area.
 * If the character array exceeds the area allowed, the array displayed is truncated.
 * @param c[] - array of characters to be displayed in the textbox
 * @param x_min - starting x postion
 * @param y_min - starting y postion, with x_min makes the starting point for text
 * @param x_max - ending x postion
 * @param y-max - ending y postion, with x_max makes the ending point for text
 */
void Game_RectText(char c[], char x_min, char y_min, char x_max, char y_max);

/**The "fight" mode of the game is called via this method. The "fight" mode display is smaller than the map to make the fight harder. The "fight" mode consists of the player dodging a set amount of enemies. If the user just played in the "fight" mode, they have to talk to another non-playable character again before playing again. Else the user goes into "fight" mode where the display is redrawn and the user has to dodge a set amount of enemies in order to win.
 * If the user just fought, they will be unable to until they interact with another non-playable character. The number of enemies in "fight" mode is reset to 0 here.
*/
void Fight(void);

/**Clears the text in the text box by filling it with spaces using the Game_RectText function.
 */
void ClearText(void);
/**Checks if the user wins the game by avoiding a set amount of enemies. If the user wins the game by avoiding a set amount of enemies, then the gamesWon counter increments and the user is placed back onto the map.
 */
void UpdateStats(void);

/**Checks what the object in front of the player is. Objects that can be found are '#'(walls), '@'(non-playable chracters), and 'T'(trees).
 * @param x - x position
 * @param y - y position
 */
void checkChar(char x, char y);

/**Interact with an object next to the user. It checks in all four directions around the user if there is a '@'(non-playable character). If it is a non playable character, then it goes into the fight mode of the game.
 * Otherwise the player cannot fight.
 * @param x - x position
 * @param y - y position
 */
void Interact(char x, char y);

/**Ends the currrent game and displays how many games wre won.
 * All enemies are deleted.
 * All tasks are removed and the UART receiver is unregistered.
 */
void GameOver(void);

#endif /* TESTGAME_H_ */
