/**
 * The game is called 'testgame' due to how it was named when testing how to make a game over UART and not really having another name for it. This class is used to create a game where the user can walk around on a map which is made by the map class and interact with non playable characters.
 * Interacting with the other characters causes the game play to change to "fight" mode where the user has to avoid a certain number of enemies. To exit the game, the user would have to press the 'enter' key in either mode or lose during fight mode.
 *
 *  @author Iryna Topchiy
 */
#include "testgame.h"

#ifndef PLAYER1_UART
#ifndef SUBSYS_UART
#error "You must define what UART number the terminal is using in system.h: #define SUBSYS_UART num"
#endif

#define PLAYER1_UART SUBSYS_UART
#endif

//The play mode map and textbox dimensions
#define MAP_WIDTH 40
#define MAP_HEIGHT 20
#define TEXTBOX_HEIGHT 30

//The "fight" mode display box sizes
#define FIGHTWIDTH 15
#define FIGHTHEIGHT 7

//enemy movement rate range
#define MIN_ENEMY_TIME 1000/(MAP_HEIGHT-1)
#define MAX_ENEMY_TIME 6000/(MAP_HEIGHT-1)
#define MIN_ENEMY_RATE 500
#define MAX_ENEMY_RATE 5000


static void Callback(int argc, char * argv[]);
static void Receiver(char c);
static void PrePlay(void);
static void Play(void);
static void Help(void);
static void MoveDown(void);
static void MoveUp(void);
static void GameOver(void);
static void MoveRight(void);
static void MoveLeft(void);
void DeleteEnemy(char_object_t * o);
void SendEnemy(void);
static void MoveDownEnemy(char_object_t * o);
static void MoveRightEnemy(char_object_t * o);
void CheckCollisionEnemy(char_object_t * enemy);
void Fight(void);
void Game_RectText(char c[], char x_min, char y_min, char x_max, char y_max);
void ClearText(void);
void UpdateStats(void);
void checkChar(char x, char y);
void Interact(char x, char y);
void MoveUpEnemy(char_object_t * o);
void MoveLeftEnemy(char_object_t * o);
void FightMoveRight(void);
void FightMoveLeft(void);
void FightMoveUp(void);
void FightMoveDown(void);
void gamePlay(void);

#define MAXENEMIES 20 //defines the max amount of enemies sent during "fight" mode
char_object_t enemies[MAXENEMIES]; //creates an array of enemies

int numEnemies;
char fighting = 0; //if fighting = 0, the user is not in "fight" mode, if fighting = 1, then  the user is in "fight" mode
int gamesWon = 0; //counter for how many games were won
int whichOne = 0; //used to choose which case the enemy directions are chosen in "fight" mode
int counter = 0; //used as a counter for where the enemies come from in "fight" mode
char isAlive = 1; //if 1, the user is alive

//period of movement in various directions for the enemy
uint16_t down_period = 0;
uint16_t right_period = 0;
uint16_t up_period = 0;
uint16_t left_period = 0;


struct testgame_t {
	char c;//charachter
	char x;//x coordninate
	char y;//y coordninate
	char status;
	uint8_t id; //id of game
};

struct testgame_t game;


void testgame_Init(void)
{
	//registers the game
	game.id = Game_Register("testgame", "Just a simple game.", PrePlay, Help);
}


void Help(void)
{
	Game_Printf("Instructions to move and interact are printed prior to game play\r\n");
	Game_Printf("There are two game modes, play and fight.\r\n");
	Game_Printf("When in play mode, the user walks around on a map and has simple interactions with the objects around.\r\n");
	Game_Printf("When in fight mode, the objective is to dodge a predefined amount of enemies to win.\r\n");
}

void Callback(int argc, char * argv[])
{
    if(argc == 0) Game_Log(game.id, "too few args");
    if(strcasecmp(argv[0],"reset") == 0) {
        Game_Log(game.id, "Scores reset");
    }else Game_Log(game.id, "not valid command");
}

void PrePlay(void) {
    Game_Printf("\f'w','a','s','d' to move. Spacebar to clear the text box. \r\n"
            "'l' to interact with NPCs \r\n"
    		"Enter to end the game \r\n"
            "Game will begin in 3 seconds.\n\r"
    		"This game really does not have much of a point. \r\n");
    //schedules the game to start in 3 seconds
    Task_Schedule(Play, 0, 3000, 0);
}

void Play(void)
{
    Game_ClearScreen();//clears the screen
    Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT); //draws the rectangle for the map
    Game_DrawRect(0,MAP_HEIGHT , MAP_WIDTH,TEXTBOX_HEIGHT); //draws the textbox below the map rectangle
    DrawMap(); //draws map
    numEnemies = 0; //number of enemies is initialized to 0
    game.x = MAP_WIDTH / 2; //x position for player
    game.y = MAP_HEIGHT /2; //y position for player
    game.c = '*'; //player character
    Game_CharXY(game.c, game.x, game.y); //draw player on map
    Game_RegisterPlayer1Receiver(Receiver); //register the receiver
    Game_HideCursor(); //hide the cursor
}

void gamePlay(void) {
    Game_ClearScreen();//clears screen
    Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT); //draws the rectangle for the map
    Game_DrawRect(0,MAP_HEIGHT , MAP_WIDTH,TEXTBOX_HEIGHT);//draws the textbox below the map rectangle
    DrawMap(); //draws the map
    Game_CharXY(game.c, game.x, game.y);//draw player on the map
   fighting = 0; //player is not in "fight" mode
}

void Receiver(char c) {
    switch (c) {
    	//movement left
        case 'a':
        case 'A':
         	if(fighting==0)
            MoveLeft();
        	else FightMoveLeft();
            break;
        //movement right
        case 'd':
        case 'D':
        	if(fighting==0)
        	MoveRight();
         	else FightMoveRight();
            break;
        //movement up
        case 'W':
        case 'w':
        	if(fighting==0)
        	MoveUp();
        	else FightMoveUp();
            break;
        //movement down
        case 'S':
        case 's':
        	if(fighting==0)
        	MoveDown();
        	else FightMoveDown();
          	break;
        //interact with objects around the player
        case 'L':
        case 'l':
        	Interact(game.x,game.y);
        	break;
        //clears textbox
        case ' ':
           ClearText();
            break;
        //end the game
        case '\r':
            GameOver();
            break;
        default:
            break;
    }
}

void MoveRight(void) {
    //check if can move right and the player can pass throught the character
    if ((game.x < MAP_WIDTH - 1)&& (canPass(++game.x,game.y) == 1)) {
        // clear location
    	--game.x;
        Game_CharXY(' ', game.x, game.y);
        game.x++;
        // update
        Game_CharXY(game.c, game.x, game.y);
    }
    else
    {
    	Game_RectText("You are unable to move right.              ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
    	//checks if the player walked into an object
    	checkChar(game.x,game.y);
    	--game.x;
    }
}

void MoveLeft(void)
{
    //check if can move left and the player can pass throught the character
    if ((game.x > 1)&&(canPass(--game.x,game.y) == 1)) {
    	++game.x;
        Game_CharXY(' ', game.x, game.y);
        game.x--;
        Game_CharXY(game.c, game.x, game.y);
    }
    else
      {
    	Game_RectText("You are unable to move left.              ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
    	//checks if the player walked into an object
    	checkChar(game.x,game.y);
    	++game.x;
      }
}

void MoveDown(void)
{
    //check if can move down and the player can pass throught the character
    if ((game.y < MAP_HEIGHT - 1)&& (canPass(game.x,++game.y) == 1))
    {
    	--game.y;
        Game_CharXY(' ', game.x, game.y);
        game.y++;
        Game_CharXY(game.c, game.x, game.y);
    }
    else
    {
    	Game_RectText("You are unable to move down.              ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
    	//checks if the player walked into an object
    	checkChar(game.x,game.y);
    	--game.y;
    }
}

void MoveUp(void)
{
    //check if can move up and the player can pass throught the character
    if ((game.y > 1)&& (canPass(game.x,--game.y) == 1))
    {
       	 ++game.y;
    	 Game_CharXY(' ', game.x, game.y);
       	 game.y--;
    	 Game_CharXY(game.c, game.x, game.y);
    }
    else
      {
		Game_RectText("You are unable to move up.                         ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
    	//checks if the player walked into an object
    	checkChar(game.x,game.y);
    	++game.y;
      }
}

void FightMoveRight(void)
{
    if (game.x < FIGHTWIDTH - 1)
    {
       Game_CharXY(' ', game.x, game.y);
       //update x position
       game.x++;
       //reprint
       Game_CharXY(game.c, game.x, game.y);
    }
}

void FightMoveLeft(void)
{
    if (game.x > 1)
    {
        Game_CharXY(' ', game.x, game.y);
        //update x position
        game.x--;
        //reprint
        Game_CharXY(game.c, game.x, game.y);
    }
}

void FightMoveDown(void)
{
    if (game.y < FIGHTHEIGHT - 1)
    {
        Game_CharXY(' ', game.x, game.y);
        //update y position
        game.y++;
        //reprint
        Game_CharXY(game.c, game.x, game.y);
    }
}

void FightMoveUp(void)
{
    if (game.y > 1)
    {
     	 Game_CharXY(' ', game.x, game.y);
    	 // update y position
    	 game.y--;
    	 // reprint
    	 Game_CharXY(game.c, game.x, game.y);
    }
}

void MoveDownEnemy(char_object_t * o)
{
    if (o->y < FIGHTHEIGHT - 1) {
        // clear location
        Game_CharXY(' ', o->x, o->y);
        // update y position
        o->y++;
        // reprint
        Game_CharXY(o->c, o->x, o->y);
        //check if the player collided with the enemy
        CheckCollisionEnemy(o);
    }
}

void MoveUpEnemy(char_object_t * o)
{
    if (o->y >1) {
        // clear location
        Game_CharXY(' ', o->x, o->y);
        // update y position
        o->y--;
        // reprint
        Game_CharXY(o->c, o->x, o->y);
        //check if the player collided with the enemy
        CheckCollisionEnemy(o);
    }
}

void MoveRightEnemy(char_object_t * o)
{
    if (o->x < FIGHTWIDTH - 1) {
        // clear location
        Game_CharXY(' ', o->x, o->y);
        // update x position
        o->x++;
        // reprint
        Game_CharXY(o->c, o->x, o->y);
        //check if the player collided with the enemy
        CheckCollisionEnemy(o);
    }
}

void MoveLeftEnemy(char_object_t * o)
{
    if (o->x > 1) {
        // clear location
        Game_CharXY(' ', o->x, o->y);
        // update x position
        o->x--;
        // reprint
        Game_CharXY(o->c, o->x, o->y);
        //check if the player collided with the enemy
        CheckCollisionEnemy(o);
    }
}

void SendEnemy(void) {
    char x;
    char y;
    volatile uint8_t i;
    char_object_t * enemy = 0;
    counter++;

    //Counts up to whichOne*5, whichOne determines what case the enemy is being sent from. After whichOne cycles through the cases, it is set to the first case. Therefore, 5 enemies are sent from each case.
    if(counter >= (whichOne*5))whichOne++; //after 5 enemies are sent from a case, it goes to the next case
    if(whichOne == 8)whichOne=1;//circular cycling through cases
    if(counter >= MAXENEMIES)counter=1; //circular cycling through counting up

    //case statements are used to decide where the enemies come from during "fight" mode
    switch(whichOne){
            		case(1):
            		//left
            			//gets a random starting point from the right side of the rectangle
            		    //sets the left period for the enemy to move
            			y= random_int(1, FIGHTHEIGHT-1);
            			left_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
            			//start on the right side
            			x = FIGHTWIDTH - 1;

            		break;
            		case(2):
            		//up
                    	//gets a random starting point from the bottom side of the rectangle
                    	//sets the up period for the enemy to move
            			x= random_int(1, FIGHTWIDTH-1);
            			up_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
            			//start on the bottom of the rectangle
            			y = FIGHTHEIGHT-1;

            		break;
            		case(3):
            		//right
						//gets a random starting point from the left side of the rectangle
		                //sets the right period for the enemy to move
            			y= random_int(1, FIGHTHEIGHT-1);
                		right_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
                		//starts on the left part of the rectangle
            			x = 1;

            		break;
            		case(4):
            		//down
						//gets a random starting point from the top side of the rectangle
		                //sets the down period for the enemy to move
            			x= random_int(1, FIGHTWIDTH-1);
            			down_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
            			//starts at the top of the rectangle
            			y= 1;

            		break;
            		case(5):
            		//right down
						//gets a random starting point from the top side of the rectangle
		                //sets the down and right period for the enemy to move
            			x = random_int(1, FIGHTWIDTH / 2);
            			down_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
            			right_period = down_period * (FIGHTHEIGHT - 1) / (FIGHTWIDTH - 1 - x);
            			right_period = random_int(right_period, right_period * 4);
            			//starts at the top of the rectangle
            			y = 1;

            		break;

            		case(6):
            		//left up
						//gets a random starting point from the right side of the rectangle
		                //sets the up and left period for the enemy to move
            			y = random_int(1, FIGHTHEIGHT / 2);
            			up_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
            			left_period = up_period * (FIGHTWIDTH - 1) / (FIGHTWIDTH - 1 - y);
            			left_period = random_int(left_period, left_period * 4);
            			//starts at the right side of the rectangle
            			x = FIGHTWIDTH-1;

            		break;

            		case(7):
            		//left down
						//gets a random starting point from the right side of the rectangle
		                //sets the down and left period for the enemy to move
            		    y = random_int(1, FIGHTHEIGHT / 2);
            			down_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
            			left_period = down_period * (FIGHTHEIGHT - 1) / (FIGHTWIDTH - 1 - y);
            			left_period = random_int(left_period, left_period * 4);
            			//starts at the right of the rectangle
            			x = FIGHTWIDTH-1;
            		break;

            		case(8):
            		//right up
						//gets a random starting point from the bottom side of the rectangle
		                //sets the up and right period for the enemy to move
            			x = random_int(1, FIGHTWIDTH / 2);
            			up_period = random_int(MIN_ENEMY_TIME, MAX_ENEMY_TIME);
            			right_period = up_period * (FIGHTHEIGHT - 1) / (FIGHTWIDTH - 1 - x);
            			right_period = random_int(right_period, right_period * 4);
            			//starts at the bottom of the rectangle
            			y = FIGHTHEIGHT-1;

            		break;
            		default:
            		break;
            			}
    //checks for avaliable enemies to be sent
    for(i = 0; i < MAXENEMIES; i++) if(enemies[i].status == 0) enemy = &enemies[i];
    if(enemy) //if enemy is available
    {
        enemy->status = 1; //enemy is attacking
        enemy->x = x; //enemy x position
        enemy->c = 'x'; //enemy is an 'x' character
        enemy->y = y; //enemy y position
        switch(whichOne)
        {
        //the tasks below are scheduled to make the enemy move in the direction specified in the cases above
        //the case numbers here are the same as the ones above
        	case(1):
				   Task_Schedule((task_fn_t)MoveLeftEnemy, enemy, left_period, left_period);
		    break;
        	case(2):
					Task_Schedule((task_fn_t)MoveUpEnemy, enemy, up_period, up_period);
           	break;
        	case(3):
				    Task_Schedule((task_fn_t)MoveRightEnemy, enemy, right_period, right_period);
        	        Task_Remove((task_fn_t)MoveLeftEnemy,0);
        	break;
        	case(4):
        			Task_Schedule((task_fn_t)MoveDownEnemy, enemy, down_period, down_period);
        	        Task_Remove((task_fn_t)MoveUpEnemy,0);
        	break;
        	case(5):
				    Task_Schedule((task_fn_t)MoveDownEnemy, enemy, down_period, down_period);
					Task_Schedule((task_fn_t)MoveLeftEnemy, enemy, left_period, left_period);
					Task_Remove((task_fn_t)MoveRightEnemy,0);
					Task_Remove((task_fn_t)MoveUpEnemy,0);
            break;
        	case(6):
					Task_Schedule((task_fn_t)MoveUpEnemy, enemy, up_period, up_period);
					Task_Schedule((task_fn_t)MoveLeftEnemy, enemy, left_period, left_period);
					Task_Remove((task_fn_t)MoveRightEnemy,0);
					Task_Remove((task_fn_t)MoveDownEnemy,0);
        	break;
        	case(7):
					Task_Schedule((task_fn_t)MoveDownEnemy, enemy, down_period, down_period);
					Task_Schedule((task_fn_t)MoveLeftEnemy, enemy, left_period, left_period);
					Task_Remove((task_fn_t)MoveUpEnemy,0);
					Task_Remove((task_fn_t)MoveRightEnemy,0);
		    break;
        	case(8):
    				Task_Schedule((task_fn_t)MoveRightEnemy, enemy, right_period, right_period);
        			Task_Schedule((task_fn_t)MoveUpEnemy, enemy, up_period, up_period);
    			    Task_Remove((task_fn_t)MoveDownEnemy,0);
    				Task_Remove((task_fn_t)MoveLeftEnemy,0);
            break;
          }
        Game_CharXY('x', x, y);
        numEnemies++; //increment number of enemies
        UpdateStats(); //check if the user won yet or not
    }

    //if the number of enemies seen during "fight" mode is less than the max number of enemies allowed
    if(numEnemies < MAXENEMIES)
    {
    	//schedule a task to send an enemy at a random rate
    	Task_Schedule(SendEnemy, 0, random_int(MIN_ENEMY_RATE, MAX_ENEMY_RATE), 0);
    }
}

void DeleteEnemy(char_object_t * o) {
    if(o) o->status = 0; //set the status of the enemy to 0 to be in not an attacking state
}

void CheckCollisionEnemy(char_object_t * enemy)
{
	//if the player and enemy are on the same spot at the same time, they collide
	if ((enemy->x == game.x) && (enemy->y == game.y))
    {
		isAlive = 0; //player had a collision with an enemy
        DeleteEnemy(enemy); //delete the enemy
        //if the player got hit by an enemy
        if(isAlive == 0)
        {
        	//puts a '%' where the the user died in game
			Game_CharXY('%', enemy->x, enemy->y);
			GameOver(); //game over
        }
    }
}

void Game_RectText(char c[], char x_min, char y_min, char x_max, char y_max)
{
    volatile uint8_t x, y;
    char temp;
    int j = 0;
    int stringLength = strlen(c); //get the lengh of the character array/string
	temp = c[j];
    for(y = y_min; y < y_max; y++)
    {
    	Game_CharXY(' ', x_min, y);
        for(x = x_min + 1; x < x_max; x++)
        {
            UART_WriteByte(PLAYER1_UART, temp); //print the character
            j++;
        	temp = c[j];
        	//break if the string does not fit in the textbox which results in the string in the textbox to be truncated
        	if(j > stringLength) break;
        }
    	if(j > stringLength) break;
    }
}

void Fight(void)
{
	volatile uint8_t i;
	//if not supposed to be in fight more or all of the enemies where played
	if((fighting == 0)||(numEnemies >= MAXENEMIES))
	{
		Game_RectText("You just played, please wait, or try again later.",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
		DeleteEnemy(0); //delete any enemies
	    Task_Remove(SendEnemy, 0); //remove send enemy tasks
	    numEnemies = 0; //resets the number of enemies to 0
	    fighting = 0;//not in fight mode
	    for(i = 0; i < MAXENEMIES; i++) enemies[i].status = 0; //set enemy status to 0 to make it available for use
	}
	else
	{
	   Game_ClearScreen();//clear screen
	   Game_DrawRect(0, 0, FIGHTWIDTH, FIGHTHEIGHT); //draw rectangle for "fight" mode
	   game.x = FIGHTWIDTH / 2; //x position for player
	   game.y = FIGHTHEIGHT /2; //y position for player
	   Game_CharXY(game.c, game.x, game.y); //draw player's character
	   Game_HideCursor(); //hide cursor
	   Task_Schedule(SendEnemy, 0, 3000, 0); //schedule to send an enemy in 3 seconds
	}
}

void ClearText(void)
{
	//puts spaces into the textbox to clear
    Game_RectText("                                                                          ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
}

void UpdateStats(void)
{
	//if the number of enemies seen during the fight mode reaches the max enemies allowed per fight
	if(numEnemies >= MAXENEMIES)
	{
		Game_RectText("You win!",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1); //display that the user won in the textbox
		DeleteEnemy(0); //delete enemies
		Task_Remove(SendEnemy, 0);//remove all send enemy tasks
		gamesWon++; //increment games won
		gamePlay(); //go back to play mode
	 }
}

void checkChar(char x, char y)
{
	char temp;
	temp = getMapChar(x,y); //gets the character in front of the player
	if(temp == '@') //checks if the character is a non-playable character
	{
		Game_RectText("Hey look, an other character... It's rude to walk into people. ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
	}
	else if(temp == '#') //checks if the character is a wall
	{
		Game_RectText("That is a wall. You are unable to walk through it. ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
	}
	else if(temp == 'T') //checks if the charater is a tree
	{
		Game_RectText("That is a tree. You still can't walk through it, try going around. ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
	}
}

void Interact(char x, char y)
{
	char cr,cl,cu,cd;
	//checks in all directions if there is a non playable character
	cr = getMapChar(--game.x,game.y);
	++game.x;
	cl = getMapChar(++game.x,game.y);
	--game.x;
	cu = getMapChar(game.x,++game.y);
	--game.y;
	cd = getMapChar(game.x,--game.y);
	++game.y;
	//if there is a '@'(non-playable character) in any of the four directions around the user
	if((cl=='@')||(cd=='@')||(cr == '@')||(cu == '@'))
	{
		ClearText(); //clears testbox
		Game_RectText("Oh, hey there. Wanna play a game of sorts? ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
		fighting = 1; //in "fight" mode
		Fight(); //"fight" mode called
	}
	else
	{
		ClearText(); //clears textbox
		Game_RectText("You can't fight right now. ",1,MAP_HEIGHT+1 , MAP_WIDTH-1,TEXTBOX_HEIGHT-1);
	}
}

void GameOver(void)
{
	DeleteEnemy(0); //delete the enemies
    Task_Remove(SendEnemy, 0);//remove all send enemy tasks
    Game_CharXY('\r', 0, TEXTBOX_HEIGHT + 1);//returns to the row below the textbox
    Game_Printf("Game Over! Well I guess you tried? \r\n");
    Game_Printf("You won %d games!",gamesWon);//displays how many games were won
    Game_UnregisterPlayer1Receiver(Receiver); //unregisters the receiver
    Game_ShowCursor(); //shows cursor
}
