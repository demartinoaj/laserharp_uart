/**
 * Header file for the map.c class. The map class is used to create a predefined ASCII map, check is the charachter in front of the player can be passed through or not, and get the character at a specified position.
 * @author: Iryna Topchiy
 */

#ifndef MAP_H_
#define MAP_H_



#include "library.h"
#include "system.h"

void DrawMap(void);
int canPass(char xpos, char ypos);
char getMapChar(char x, char y);

#define MAP_WIDTH 40 //defines the width of the map
#define MAP_HEIGHT 20 //defines the height of the map
/**
 * mapArray is an array which is used for the map. The '@' characters are non=playable characters in the game, the '#' characters are walls in the game, and the 'T' characters are trees in the game.
 */
char mapArray[MAP_HEIGHT][MAP_WIDTH] = {
{' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{' ','@','#',' ',' ',' ','T','T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T',' ',' ',' ',' ',' ','T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T','@',' ',' ',' ',' ','T',' ',' ','@',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ','T',' ',' ','T','T','T',' ','T',' ',' ',' ','T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{'T',' ','#',' ',' ',' ',' ','@',' ',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{'#','#','#','#','#','#','#','#','#','#',' ','#','#','#','#','#','#','#',' ','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','@',' ',' ',' ','#',' ',' ','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{' ',' ','#','#','#','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T',' ','#'},
{' ',' ','#',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T',' ',' '},
{' ',' ','#',' ','#',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','@',' ',' ',' ',' ',' ',' ',' ','T','T','T'},
{' ',' ','#',' ','#','@',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
{' ',' ','#',' ','#','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T',' ',' ',' ',' ','T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T','T',' '},
{' ',' ','#',' ',' ',' ',' ',' ',' ','T',' ',' ','T',' ',' ',' ',' ',' ',' ','T',' ',' ',' ',' ','T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T',' '},
{'T',' ','#','#','#','#','#',' ',' ',' ','T','T',' ',' ',' ',' ',' ',' ',' ','T',' ','T','T','T','T',' ',' ',' ',' ',' ',' ','T',' ',' ',' ',' ',' ',' ','T',' '},
{'T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T',' ',' ',' ','#','#',' ','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T','T',' ',' ',' ',' ',' ','T',' '},
{'T',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ','#','@',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T','T','T','T','T','T','T','T',' '},
{'T','T',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ','T',' ','#','@',' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T','T','T',' ',' ',' ',' ',' ',' '},
{'T','T','T','@',' ',' ',' ','@',' ','#',' ',' ',' ',' ',' ','#',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','T','T','@',' ','T','T',' ',' ',' '},
{'T','T','T','T','T','T','T','T','#','#','#',' ','T',' ',' ','#','#','#','#','#','T','T','T','T','T','T','T','T','T','T','T','T','T','T','T','T','T',' ','T',' '}
};


/** Draws the mapArray to create the map by iterating and printing the characters in mapArray. Uses nested for loops to iterate through the 2 dimentional mapArray and print each character.
 */
void DrawMap(void);

/** Checks if the user is able to pass through the characters in the map. Characters that are able to be walked through are spaces. '#', '@', and 'T' are characters that cannot be walked over by the user.
 * Returns a 1 if the user can pass through the character.
 * Returns a 0 if the user cannot pass through the character.
 * @param xpos - x position in the mapArray
 * @param ypos - y position in the mapArray
 * @return 1 or 0
 */
int canPass(char ypos, char xpos);

/** Gets the character at the specified position.
 * @param y - y position in the mapArray
 * @param x - x position in the mapArray
 * @return mapArray[x][y] - returns the character at the (x,y) coordinate in the array
 */
char getMapChar(char y, char x);

#endif /* MAP_H_ */
