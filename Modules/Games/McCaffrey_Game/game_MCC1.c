/**
 * File:   game_MCC1.h
 *
 *  Created on: Mar 22, 2016
 *      @author: Michael McCaffrey
 */

#include "game_MCC1.h"

// define map size
#define MAP_WIDTH 180
#define MAP_HEIGHT 48
#define LOGO_START 20

// defines for y positions of menu options
#define TitlePlay 35
#define TitleOptions 36
#define TitleHelp 37
#define GravityToggle 35
#define Back 36

MCC_dir_t dir; /**< Iteration of direction enumeration*/

char g = 1; /**< value of acceleration for gravity */

MCC_object_t player; /**< Iteration of object representing player.*/

float Time; /**< Floating point number for total time of race*/
long TimeStart; /**< Time in milliseconds that the race begins*/

goal goals[5]; /**< Array of goal structures which contains a goal for each level*/

/// game structure
struct MCC_game_t {
    uint8_t id; /**< ID of game*/
    uint16_t score; /**< Score of game*/
};

static struct MCC_game_t game; /**< Iteration of game structure*/

MCC_menu_t menu; MCC_dir_t dir; /**< Iteration of menu structure*/

void MCC1_Init(void) {
    /// Register the module with the game system and give it the name "MCC1"
    game.id = Game_Register("MCC1", "game", TitleMenu, Help);
    /// Register a callback with the game system.
    Game_RegisterCallback(game.id, Callback);
}

void Help(void) {
	if (menu.context == Title){
		Terminal_CursorXY(0,56, 39);
		Game_Printf("Use i,j,k,l to accelerate. Navigate to the X as quickly as possible.\r\n");
	}
}

void Callback(int argc, char * argv[]) {
    // "play" and "help" are called automatically so just process "reset" here
    if(argc == 0) Game_Log(game.id, "too few args");
    if(strcasecmp(argv[0],"reset") == 0) {
        // reset scores
        game.score = 0xFFFF;
        Game_Log(game.id, "Scores reset");
    }else Game_Log(game.id, "command not supported");
}

void GameInit(){
	// initialize player
	player.avatar[0] = '<';
	player.avatar[1] = '=';
	player.avatar[2] = '>';
	player.avatar[3] = 0;
	player.x = 2;
	player.y = 45;
	player.length = 3;
	player.height = 1;
	player.xvel = 0;
	player.yvel = 0;
	player.level = 0;

	// create goals
	goals[0].x = 165;
	goals[0].y = 22;
	goals[1].x = 170;
	goals[1].y = 27;
	goals[2].x = 150;
	goals[2].y = 40;
	goals[3].x = 175;
	goals[3].y = 3;
}

void MenuInit(){
	// initialize menu
	menu.cursor = '>';
	menu.cursor_x = 89;
	menu.cursor_y = 35;
	menu.cursor_xmax = 89;
	menu.cursor_xmin = 89;
	menu.cursor_ymax = 37;
	menu.cursor_ymin = 35;
}

char startup = 1; /**< Flag indictating that the title menu is being entered for the first time*/

void TitleMenu(void) {
	if (startup){
	Game_RegisterPlayer1Receiver(MenuReceiver);
	startup = 0;
	}
	menu.context = Title;
	GameInit();
	MenuInit();
	// Prepare screen for menu
	Terminal_SetColor(0,Reset_all_attributes);
	Game_HideCursor();
	Terminal_ClearScreen(0);
	Game_DrawRect(0, 0, MAP_WIDTH+1, MAP_HEIGHT+1);

	// Draw super awesome logo, printf can only handle so many characters at once, so first few strings are divided up
	Terminal_SetColor(0, ForegroundCyan);
	Terminal_CursorXY(0,LOGO_START,2);
	Game_Printf(" ________  ___  ___  ________  _______   ________          ________  ");
	Game_Printf("___       __   _______   ________  ________  _____ ______   _______             ");
	Terminal_CursorXY(0,LOGO_START,3);
	Game_Printf("|\\   ____\\|\\  \\|\\  \\|\\   __  \\|\\  ___ \\ |\\   __  \\       ");
	Game_Printf(" |\\   __  \\|\\  \\     |\\  \\|\\  ___ \\ |\\   ____\\|\\   __  \\|\\   _ \\  _   \\|\\  ___ \\            ");
	Terminal_CursorXY(0,LOGO_START,4);
	Game_Printf("\\ \\  \\___|\\ \\  \\\\\\  \\ \\  \\|\\  \\ \\   __/|\\ \\  \\|\\  \\       ");
	Game_Printf("\\ \\  \\|\\  \\ \\  \\    \\ \\  \\ \\   __/|\\ \\  \\___|\\ \\  \\|\\  \\ \\  \\\\\\__\\ \\  \\ \\   __/|           ");
	Terminal_CursorXY(0,LOGO_START,5);
	Game_Printf(" \\ \\_____  \\ \\  \\\\\\  \\ \\   ____\\ \\  \\_|/_\\ \\   _  _\\       \\ ");
	Game_Printf("\\   __  \\ \\  \\  __\\ \\  \\ \\  \\_|/_\\ \\_____  \\ \\  \\\\\\  \\ \\  \\\\|__| \\  \\ \\  \\_|/__         ");
	Terminal_CursorXY(0,LOGO_START,6);
	Game_Printf("  \\|____|\\  \\ \\  \\\\\\  \\ \\  \\___|\\ \\  \\_|\\ \\ \\  \\\\  \\|     ");
	Game_Printf("  \\ \\  \\ \\  \\ \\  \\|\\__\\_\\  \\ \\  \\_|\\ \\|____|\\  \\ \\  \\\\\\  \\ \\  \\    \\ \\  \\ \\  \\_|\\ \\        ");
	Terminal_CursorXY(0,LOGO_START,7);
	Game_Printf("    ____\\_\\  \\ \\_______\\ \\__\\    \\ \\_______\\ \\__\\\\ _\\        \\");
	Game_Printf(" \\__\\ \\__\\ \\____________\\ \\_______\\____\\_\\  \\ \\_______\\ \\__\\    \\ \\__\\ \\_______\\       ");
	Terminal_CursorXY(0,LOGO_START,8);
	Game_Printf("   |\\_________\\|_______|\\|__|     \\|_______|\\|__|\\|__|        \\|__|\\|");
	Game_Printf("__|\\|____________|\\|_______|\\_________\\|_______|\\|__|     \\|__|\\|_______|       ");
	Terminal_CursorXY(0,LOGO_START,9);
	Game_Printf("   \\|_________|                                                       ");
	Game_Printf("                          \\|_________|                                         ");
	Terminal_CursorXY(0,LOGO_START,10);
	Game_Printf("                                                                                                                                                     ");
	Terminal_CursorXY(0,LOGO_START,11);
	Game_Printf("                                                                                                                                                     ");
	Terminal_CursorXY(0,LOGO_START,12);
	Game_Printf("                                            ________  ________  ________  ________  _______           ________  ________  ________  _______          ");
	Terminal_CursorXY(0,LOGO_START,13);
	Game_Printf("                                           |\\   ____\\|\\   __  \\|\\   __  \\|\\   ____\\|\\  ___ \\         |\\   __  \\|\\   __  \\|\\   ____\\|\\  ___ \\         ");
	Terminal_CursorXY(0,LOGO_START,14);
	Game_Printf("                                           \\ \\  \\___|\\ \\  \\|\\  \\ \\  \\|\\  \\ \\  \\___|\\ \\   __/|        \\ \\  \\|\\  \\ \\  \\|\\  \\ \\  \\___|\\ \\   __/|        ");
	Terminal_CursorXY(0,LOGO_START,15);
	Game_Printf("                                            \\ \\_____  \\ \\   ____\\ \\   __  \\ \\  \\    \\ \\  \\_|/__       \\ \\   _  _\\ \\   __  \\ \\  \\    \\ \\  \\_|/__      ");
	Terminal_CursorXY(0,LOGO_START,16);
	Game_Printf("                                             \\|____|\\  \\ \\  \\___|\\ \\  \\ \\  \\ \\  \\____\\ \\  \\_|\\ \\       \\ \\  \\\\  \\\\ \\  \\ \\  \\ \\  \\____\\ \\  \\_|\\ \\     ");
	Terminal_CursorXY(0,LOGO_START,17);
	Game_Printf("                                               ____\\_\\  \\ \\__\\    \\ \\__\\ \\__\\ \\_______\\ \\_______\\       \\ \\__\\\\ _\\\\ \\__\\ \\__\\ \\_______\\ \\_______\\    ");
	Terminal_CursorXY(0,LOGO_START,18);
	Game_Printf("                                              |\\_________\\|__|     \\|__|\\|__|\\|_______|\\|_______|        \\|__|\\|__|\\|__|\\|__|\\|_______|\\|_______|    ");
	Terminal_CursorXY(0,LOGO_START,19);
	Game_Printf("                                              \\|_________|                                                                                           ");
	Terminal_CursorXY(0,LOGO_START,20);
	Game_Printf("                                                                                                                                                     ");
	Terminal_CursorXY(0,LOGO_START,LOGO_START);
	Game_Printf("                                                                                                                                                     ");
	Terminal_SetColor(0, ForegroundRed);
	Terminal_CursorXY(0,LOGO_START,22);
	Game_Printf("                                                                                         ________  ________  ________  ________                      ");
	Terminal_CursorXY(0,LOGO_START,23);
	Game_Printf("                                                                                        |\\   ____\\|\\   __  \\|\\   __  \\|\\   __  \\                     ");
	Terminal_CursorXY(0,LOGO_START,24);
	Game_Printf("                                                                                        \\ \\  \\___|\\ \\  \\|\\  \\ \\  \\|\\  \\ \\  \\|\\  \\                    ");
	Terminal_CursorXY(0,LOGO_START,25);
	Game_Printf("                                                                                         \\ \\_____  \\ \\  \\\\\\  \\ \\  \\\\\\  \\ \\  \\\\\\  \\                   ");
	Terminal_CursorXY(0,LOGO_START,26);
	Game_Printf("                                                                                          \\|____|\\  \\ \\  \\\\\\  \\ \\  \\\\\\  \\ \\  \\\\\\  \\                  ");
	Terminal_CursorXY(0,LOGO_START,27);
	Game_Printf("                                                                                            ____\\_\\  \\ \\_______\\ \\_______\\ \\_______\\                 ");
	Terminal_CursorXY(0,LOGO_START,28);
	Game_Printf("                                                                                           |\\_________\\|_______|\\|_______|\\|_______|                 ");
	Terminal_CursorXY(0,LOGO_START,29);
	Game_Printf("                                                                                           \\|_________|                                              ");

	// Add menu
	Terminal_SetColor(0, ForegroundWhite);
	Terminal_CursorXY(0,90,35);
    Game_Printf("Play");
    Terminal_CursorXY(0,90,36);
    Game_Printf("Options");
    Terminal_CursorXY(0,90,37);
    Game_Printf("Help");
    Game_CharXY(menu.cursor, menu.cursor_x, menu.cursor_y);

}

void MenuReceiver(char c) {
    switch (c) {
        case 'i':
        case 'I':
            MoveCursor(Up);
            break;
        case 'k':
        case 'K':
        	MoveCursor(Down);
            break;
        case ' ':
            MenuSelect(menu.cursor_y);
            break;
        default:
            break;
    }
}

static void MenuSelect(char choice){
	switch (menu.context){

	case Title:
		switch (choice){
		case TitlePlay: Task_Schedule(Play, 0, 0, 0); break;
		case TitleOptions: Task_Schedule(Options, 0, 0, 0); break;
		case TitleHelp: Task_Schedule(Help, 0, 0, 0); break;
		default: break;
		}

	case OptionMenu:
		switch (choice){
		case GravityToggle: {
			g ^= 1;
			Terminal_CursorXY(0, 98,35);
			if(g) Game_Printf("ON ");
			else Game_Printf("OFF");
			break;
		}
		case Back: Task_Schedule(TitleMenu,0,0,0); break;
		}
	}
};

void Game_DrawMap(){
	unsigned volatile int x;
	unsigned volatile int y;
	switch (player.level){
	case 0: Game_SetColor(BackgroundBlue); break;
	case 1: Game_SetColor(BackgroundYellow); break;
	case 2: Game_SetColor(BackgroundGreen); break;
	}

	// All maps have rectangular perimeter
	for (x = 179; x; x--){
		Game_CharXY(' ', x, 47);
		Game_CharXY(' ', x, 1);
	}
	for (y = 46; y; y--){
		Game_CharXY(' ', 1, y);
		Game_CharXY(' ', 179, y);
	}

	if(player.level == 0){
		/**
		 * First level is mostly just a demonstration / tutorial level.
		 * Shift in if statement filters out all but x coordinates greater than 127.
		 * X coordinates greater than 127 are passed to inside FOR loop which generates
		 * elevated platform.
		 */
		for (x = 179; x; x--){
				if (x >> 7) for (y = 47; y>=23; y--){
				Game_CharXY(' ', x, y);
				}
		}
	}

	unsigned volatile char k;

	if(player.level == 1){

		/**
		 * Draw diagonal walls (first FOR loop).
		 * Every iteration leaves a gap on the opposite end as the previous one.
		 * Game_CharXY x coordinate has dependency on y which creates diagonal lines.
		 */
		for (x = 25; x; x-=5){
			k = (x&1)? 1 : 0;
				for (y = 40 + 7*k; y - 7*k; y--){
					Game_CharXY(' ', y  - 20 + x*6, y);
				}
		}
	}

	if(player.level == 2){

		unsigned volatile char k;
		unsigned volatile char n;

		/**
		 * Draw 7 vertical walls (FOR loop #1).
		 * Each iteration stops 5 blocks shorter of the ceiling (y - 5*n - 5 in FOR loop #2).
		 * Every odd iteration leaves a 5 block gap to the floor (y = 46 - k*5 in FOR loop #2)
		 * K is 0 for all even iterations and 1 for odd iterations
		 */
			for (n = 0; n < 7; n++){ // n = 0 to 7
				k = (n&1)? 1 : 0; // k is 1 when n is odd
				for (y = 46 - k*5; y - 5*n - 5; y--){
						Game_CharXY(' ', 20 + n*20, y);
					}
			}

		/**
		 * Draw 7 horizontal walls (FOR loop #1).
		 * Each iteration stops 20 blocks shorter of the left wall (x - 20*n - 20 in FOR loop #2).
		 * Every odd iteration leaves a 20 block gap to the floor (y = 180 - k*20 in FOR loop #2)
		 * K is 1 for all even iterations and 0 for odd iterations
		 */
			for (n = 0; n < 7; n++){
				k = (n&1)? 0 : 1;
				for (x = 180 - k*20; x - 20*n - 20; x--){
					Game_CharXY(' ', x - 1, 6 + n*5);
				}
			}
		}

	Game_SetColor(Reset_all_attributes);
	Game_CharXY('X', goals[player.level].x, goals[player.level].y);

}

void Options(void){
	menu.context = OptionMenu;
	menu.cursor_ymax = 36;
	Terminal_ClearScreen(0);
	Game_DrawRect(0, 0, MAP_WIDTH+1, MAP_HEIGHT+1);
	Terminal_CursorXY(0,90,35);
    Game_Printf("Gravity");
    Terminal_CursorXY(0,90,36);
    Game_Printf("Back");
    Terminal_CursorXY(0, 98,35);
    if(g) Game_Printf("ON "); else Game_Printf("OFF "); // display gravity status
    Game_CharXY(menu.cursor, menu.cursor_x, menu.cursor_y);
};

void Play(void) {
	Game_UnregisterPlayer1Receiver(MenuReceiver);

    Game_ClearScreen();

    Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT);
    Game_DrawMap();

    MCC_DrawTile(player.avatar, player.x, player.y);

    Game_RegisterPlayer1Receiver(GameReceiver);
    TimeStart = TimeNow(); // initialize timer
    Task_Schedule(Motion, 0, 0, 1);
    Task_Schedule(Gravity, 0, 0, 1000);

}

void Gravity(){
	Accelerate(&player, Down, g);
}

void MCC_DrawTile(char tile[], char x, char y){
	volatile unsigned int i = 0;
	while(tile[i]){
		Game_CharXY(tile[i], x, y);
		x++;
		i++;
	}
}

char solveUP(char level, char x, char y){

	unsigned volatile char i;
	unsigned volatile char j;

	if (y == 1)
		return 1;

	if(player.level == 0){
		for (i = 179; i; i--){
			if (i >> 7) for (j = 47; j>=23; j--){
				if (x == i && y-1 == j)
					return 1;
			}
		}
	}

	signed volatile char k;

	if(player.level == 1){

		for (i = 25; i; i-=5){
			k = (i&1)? 1 : 0;
			for (j = 40 + 7*k; j - 7*k; j--){
				if ((x == j  - 20 + i*6 && y == j) || (x + 1 == j  - 20 + i*6 && y == j) || (x + 2 == j  - 20 + i*6 && y == j)) return 1;
			}
		}
	}

	if(player.level == 2){
		unsigned volatile char k;
		unsigned volatile char n;
		for (n = 0; n < 7; n++){
			k = (n&1)? 1 : 0;
			for (j = 46 - k*5; j - 5*n - 5; j--){
				if (x == 20 + n*20 && y - 1 == j || x + 1== 20 + n*20 && y - 1 == j || x + 2 == 20 + n*20 && y - 1 == j ) return 1;
			}
		}
		for (n = 0; n < 7; n++){
			k = (n&1)? 0 : 1;
			for (i = 180 - k*20; i - 20*n - 20; i--){
				if (x == i - 1 && y == 6 + n*5 || x + 1 == i - 1 && y == 6 + n*5 || x + 2 == i - 1 && y == 6 + n*5) return 1;
			}
		}
	}

	return 0;
}

char solveDOWN(char level, char x, char y){

	unsigned volatile char i;
	unsigned volatile char j;

	if (y == 47)
		return 1;

	if(player.level == 0){
		for (i = 179; i; i--){
			if (i >> 7) for (j = 47; j>=23; j--){
				if (x == i && y == j)
					return 1;
			}
			else if (y == 47)
				return 1;
		}
	}

	signed volatile char k;

	if(player.level == 1){
		for (i = 25; i; i-=5){
			k = (i&1)? 1 : 0;
			for (j = 40 + 7*k; j - 7*k; j--){
				if ((x == j  - 20 + i*6 && y == j) || (x + 1 == j  - 20 + i*6 && y == j) || (x + 2 == j  - 20 + i*6 && y == j)) return 1;
			}
		}
	}

	if(player.level == 2){
		unsigned volatile char k;
		unsigned volatile char n;
		for (n = 0; n < 7; n++){
			k = (n&1)? 1 : 0;
			for (j = 46 - k*5; j - 5*n - 5; j--){
				if (x == 20 + n*20 && y == j || x + 1 == 20 + n*20 && y == j  || x + 2 == 20 + n*20 && y == j) return 1;
			}
		}
		for (n = 0; n < 7; n++){
			k = (n&1)? 0 : 1;
			for (i = 180 - k*20; i - 20*n - 20; i--){
				if (x == i - 1 && y == 6 + n*5 || x == i - 1 && y == 6 + n*5 || x +2 == i - 1 && y == 6 + n*5) return 1;
			}
		}
	}

	return 0;
}

char solveRIGHT(char level, char x, char y){

	unsigned volatile char i;
	unsigned volatile char j;

	if (x == 179)
		return 1;

	if(player.level == 0){
		for (i = 179; i; i--){
			if (i >> 7) for (j = 47; j>=23; j--){
				if (x + 2 == i && y == j)
					return 1;
			}
			else if (x == 178)
				return 1;
		}
	}

	signed volatile char k;
	if(player.level == 1){
		for (i = 25; i; i-=5){
			k = (i&1)? 1 : 0;
			for (j = 40 + 7*k; j - 7*k; j--){
				if (x == j  - 20 + i*6 && y == j) return 1;
			}
		}
	}

	if(player.level == 2){
		unsigned volatile char k;
		unsigned volatile char n;
		for (n = 0; n < 7; n++){
			k = (n&1)? 1 : 0;
			for (j = 46 - k*5; j - 5*n - 5; j--){
				if (x == 20 + n*20 && y == j) return 1;
			}
		}
		for (n = 0; n < 7; n++){
			k = (n&1)? 0 : 1;
			for (i = 180 - k*20; i - 20*n - 20; i--){
				if (x == i - 1 && y == 6 + n*5) return 1;
			}
		}
	}

	return 0;
}

char solveLEFT(char level, char x, char y){

	unsigned volatile char i;
	unsigned volatile char j;

	if (x == 1)
		return 1;

	if(player.level == 0){
			if (x == 1)
				return 1;
	}

	signed volatile char k;

	if(player.level == 1){
		for (i = 25; i; i-=5){
			k = (i&1)? 1 : 0;
			for (j = 40 + 7*k; j - 7*k; j--){
				if (x == j  - 20 + i*6 && y == j) return 1;
			}
		}
	}

	if(player.level == 2){
		unsigned volatile char k;
		unsigned volatile char n;
		for (n = 0; n < 7; n++){
			k = (n&1)? 1 : 0;
			for (j = 46 - k*5; j - 5*n - 5; j--){
				if (x == 20 + n*20 && y == j) return 1;
			}
		}
		for (n = 0; n < 7; n++){
			k = (n&1)? 0 : 1;
			for (i = 180 - k*20; i - 20*n - 20; i--){
				if (x == i - 1 && y == 6 + n*5) return 1;
			}
		}
	}

	return 0;
}

void MCC_MoveTile(MCC_object_t * target, MCC_dir_t dir){
	volatile unsigned int i;
	if (dir == Up ){
		if (solveUP(player.level, target->x, (target->y) - 1)){ // if next y position fits into map rawing scheme, return without moving
			target->yvel = 0;
			return;
		}}
	else if (dir == Down){
		if (solveDOWN(player.level, target->x, (target->y) + 1)){ // if next y position fits into map rawing scheme, return without moving
			target->yvel = 0;
			return;
		}}
	else if (dir == Left){
		if (solveLEFT(player.level, (target->x) - 1, target->y)){ // if next x position fits into map rawing scheme, return without moving
			target->xvel = 0;
			return;
		}}
	else if (dir == Right){
		if (solveRIGHT(player.level, (target->x) + 3, target->y)){ // if next x position fits into map rawing scheme, return without moving
			target->xvel = 0;
			return;
		}}

	if (dir == Right){ // because the players avatar is a tile of 3 characters, moving right requires special handling
		i = target->length - 1;
	while(target->avatar[i]){
		MoveChar(target->avatar[i], target->x + i, target->y, dir);
		i--;
		}
	}
	else {
		i = 0;
		while(target->avatar[i]){
		MoveChar(target->avatar[i], target->x + i, target->y, dir);
		i++;
		}
	}

	// update the target's position inside its data structure
	switch(dir){
	case Up: (target->y)--; break;
	case Down: (target->y)++; break;
	case Left: (target->x)--; break;
	case Right: (target->x)++; break;
	}

	// check if target is now in the goal and if so, run success function
    if ((target->x == goals[player.level].x ||
    		target->x == (goals[player.level].x) + 1 ||
			target->x == (goals[player.level].x) + 2 )
    		&& target->y == goals[player.level].y)
    	Success();
}

void Success(){

	// if there are levels remaining, increment level, draw the next map, and keep playing
	if (player.level < 2){
		player.x = 2;
		player.y = 45;
		player.xvel = 0;
		player.yvel = 0;
		player.level++;

		Game_ClearScreen();
		Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT);
		Game_DrawMap();

		MCC_DrawTile(player.avatar, player.x, player.y);

	}
	// if no maps remain, record time and end game.
	else{
		Game_UnregisterPlayer1Receiver(GameReceiver);
		Game_RegisterPlayer1Receiver(MenuReceiver);
		Time = TimeNow() - TimeStart;
		Game_ClearScreen();
		Game_DrawRect(0, 0, MAP_WIDTH, MAP_HEIGHT);
		Terminal_CursorXY(0, 87, 23);
		Game_Printf("Finish!");
		Terminal_CursorXY(0, 80, 24);
		Time /=1000;
		Game_Printf("Your time: %f seconds", Time);
		Terminal_CursorXY(0, 80, 25);
		Game_Printf("Best time: %f seconds", game.score);
		if (Time < game.score) game.score = Time;
		// return to title menu in 10 seconds
		Task_Schedule(TitleMenu, 0,10000,0);
	}
}

void GameReceiver(char c){
	switch (c) {
	        case 'i':
	        case 'I':
	            Accelerate(&player, Up, 1);
	            break;
	        case 'k':
	        case 'K':
	        	Accelerate(&player, Down, 1);
	            break;
	        case 'j':
	        case 'J':
	        	Accelerate(&player, Left, 1);
	            break;
	        case 'l':
	        case 'L':
	        	Accelerate(&player, Right, 1);
	        	break;
	        case ' ':

	            break;
	        default:
	            break;
	    }

};

void Accelerate(MCC_object_t * target, MCC_dir_t dir, unsigned int a){
	switch (dir){
	case Up: target->yvel += a; break;
	case Down: target->yvel -= a; break;
	case Left: target->xvel -=a; break;
	case Right: target->xvel +=a; break;
	}
}

void Motion(){
	static unsigned int xc = 0;
	static unsigned int yc = 0;

	/* The higher velocity is, the faster xc will reach 20000/xvel.
	 * When it does, the player is moved. Therefore the higher the velocity,
	 * the higher the frequency of the avatar's movement.
	 */
	if (player.xvel > 0){
		xc+=100;
		if (xc >= 20000/player.xvel){
			MCC_MoveTile(&player, Right);
			xc = 0;
		}
	}
	if (player.xvel < 0){
		xc+=100;
		if (xc >= 20000/(-player.xvel)){
			MCC_MoveTile(&player, Left);
			xc = 0;
		}
	}
	if (player.yvel > 0){
		yc+=100;
		if (yc >= 40000/player.yvel){
			MCC_MoveTile(&player, Up);
			yc = 0;
		}
	}
	if (player.yvel < 0){
		yc+=100;
		if (yc >= 40000/(-player.yvel)){
			MCC_MoveTile(&player, Down);
			yc = 0;
		}
	}
}

void MoveChar(char target, int x, int y, MCC_dir_t dir){
		switch(dir) {
			case Up:{
				Game_CharXY(' ', x, y);

				// update
				Game_CharXY(target, x, y - 1);
				break;
			}
			case Down: {
				Game_CharXY(' ', x, y);

				// update
				Game_CharXY(target, x, y+1);
				break;
			}
			case Left: {
				Game_CharXY(' ', x, y);

				// update
				Game_CharXY(target, x-1, y);
				break;
			}
			case Right: {
				Game_CharXY(' ', x, y);

				// update
				Game_CharXY(target, x+1, y);
				break;
			}
		}
}

void MoveCursor(MCC_dir_t dir) {

    if (dir == Up && menu.cursor_y > menu.cursor_ymin) {
			// clear location
			Game_CharXY(' ', menu.cursor_x, menu.cursor_y);
			menu.cursor_y--;
			// update
			Game_CharXY(menu.cursor, menu.cursor_x, menu.cursor_y);
    }
    if (dir == Down && menu.cursor_y < menu.cursor_ymax) {
            // clear location
            Game_CharXY(' ', menu.cursor_x, menu.cursor_y);
            menu.cursor_y++;
            // update
            Game_CharXY(menu.cursor, menu.cursor_x, menu.cursor_y);
    }
    if (dir == Left && menu.cursor_y > menu.cursor_xmin) {
            // clear location
            Game_CharXY(' ', menu.cursor_x, menu.cursor_y);
            menu.cursor_x--;
            // update
            Game_CharXY(menu.cursor, menu.cursor_x, menu.cursor_y);
    }
    if (dir == Right && menu.cursor_y < menu.cursor_xmax) {
            // clear location
            Game_CharXY(' ', menu.cursor_x, menu.cursor_y);
            menu.cursor_x++;
            // update
            Game_CharXY(menu.cursor, menu.cursor_x, menu.cursor_y);
    }
    else return;
}
