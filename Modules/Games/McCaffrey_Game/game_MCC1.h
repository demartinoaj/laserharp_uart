/**
 * File:   game_MCC1.h
 *
 * @author Michael McCaffrey
 *
 */


#ifndef MCC_GAME1_H_
#define MCC_GAME1_H_

#include "system.h"
#include "random_int.h"
#include "strings.h"
#include "timing.h"

/** Enumerated direction type
*/
typedef enum{
	Up = 0,
	Down,
	Left,
	Right
}MCC_dir_t;

/**
 * Generic object type with avatar, position, velocity, and size. currently
 * only used for the player, also stores the level that the player is on
 */
typedef struct{
	char avatar[4]; /**< Object's avatar, up to 3 characters, null marks end */
	char x; /**< Current x position of object */
	char y; /**< Current y position of object */
	char length; /**< Size of object in x direction */
	char height; /**< Size of object in y direction */
	signed char xvel; /**< Velocity in x direction, signed */
	signed char yvel; /**< Velocity in y direction, unsigned */
	char level; /**< For player, the current level */
}MCC_object_t;

/**
 * Goal structure which stores the x and y coordinates of the goal
 * for each level.
 */
typedef struct{
	char x; /**< X coordinate of goal*/
	char y; /**< Y coordinate of goal*/
}goal;

/**
 * Menu structure containing all information needed to carry out menu functionality
 * Structure supports menu cursor moving in all directions, but this game only needs
 * cursor movement up and down, so xmin and xmax are fixed at the same value.
 */
typedef struct{
	char cursor; /**< Cursor's display character*/
	char cursor_x; /**< X position of cursor */
	char cursor_xmin; /**< Low limit of cursor x position */
	char cursor_xmax; /**< High limit of cursor x position */
	char cursor_y; // /**< Y position of cursor*/
	char cursor_ymin; /**< Low limit of cursor y position*/
	char cursor_ymax; /**< High limit of cursor y position*/
	enum MCC_con_t {  /**< Keeps track of the menu context when selections are made */
			Title = 0,
			OptionMenu,
			HelpMenu,
	} context;
}MCC_menu_t;



void MCC1_Init(void);

static void GameInit();
static void Callback(int argc, char * argv[]);

/**
 * Receives input for control during game
 * @param c -- input character received from the computer
 */
static void GameReceiver(char c);

/**
 * Receives input for control in menus
 * @param c -- input character received from computer
 */
static void MenuReceiver(char c);

/**
 * Draws initial position of a tile of characters
 * @param tile[] -- array of chars which make up the tile
 * @param x -- desired x position of first character of tile
 * @param y -- desired y position of first character of tile
 */
static void MCC_DrawTile(char tile[], char x, char y);

/**
 * Checks if any part of target is colliding with a wall. If so, return and reset velocity in the wall's direction.
 * Checks if any part of tagert has reached the goal. If so, calls Success function.
 * If neither, moves object one character in any direction.
 * @param target -- pointer to object structure, in this case is always the player's ship
 * @param dir -- desired direction for movement
 */
static void MCC_MoveTile(MCC_object_t * target, MCC_dir_t dir);

/**
 * Changes the frequency of movements in a particular direction by changing the target's x or y velocity.
 * @param target -- pointer to object structure to ace;erate, always the player's ship
 * @param dir -- desired direction of movement
 * @param a -- value of acceleration so that gravity and ship acceleration can have different values
 */
static void Accelerate(MCC_object_t * target, MCC_dir_t dir, unsigned int a);

/**
 * Called every millisecond, moves target. Frequency of movements depend on its x and y velocity
 * @param data -- data recieved from the computer
 */
static void Motion();

/**
 * Called every half second, automatically accelerates ship downward. No need to check if target is
 * on floor because contact with floor resets y velocity to 0.
 */
static void Gravity();

/**
 * Draws the visual map and creates collision map according to the level that is on. Level is
 * checked inside function when DrawMap() is called, so level is not passed into it.
 */
static void Game_DrawMap();

/**
 * Updates level, moves player to start, and redraws map if there is another level available,
 * otherwise ends game and displays time.
 */
static void Success();

/**
 * Displays and handles controls for title menu. Menu receiver is initialized if first time
 * entering title menu.
 */
static void TitleMenu();

/**
 * Updates game based on menu selection, either by moving to a new menu, toggling an option,
 * or starting the game.
 * @param c -- y position of the cursor when spacebar is pressed which is representative of the
 * menu selection.
 */
static void MenuSelect(char c);

/**
 * Starts the game by drawing the map, placing the player's ship, starting the timer, registering
 * the gampelay char receiver, and scheduling the motion and gravity tasks
 */
static void Play();

/**
 * Displays and handles controls for options menu.
 */
static void Options();

/**
 * Prints game instructions underneath title menu
 */
static void Help();

/**
 * Moves menu cursor up or down.
 * @param dir -- desired direction to move cursor
 */
static void MoveCursor(MCC_dir_t dir);

/**
 * Moves a character.
 * @param target -- desired character to move
 * @param x -- current x position
 * @param y -- current y position
 * @param dir -- desired direction
 */
static void MoveChar(char target, int x, int y, MCC_dir_t dir);

/**
 * Checks if the target's intended new position is used by the map drawing formula.
 * If the new position belongs to a wall, function returns a 1, which forces the
 * MCC_MoveTile function to return without carrying out any motion.
 * @param level -- current level
 * @param x -- current x position
 * @param y -- current y position
 */
char solveUP(char level, char x, char y);

/**
 * Checks if the target's intended new position is used by the map drawing formula.
 * If the new position belongs to a wall, function returns a 1, which forces the
 * MCC_MoveTile function to return without carrying out any motion.
 * @param level -- current level
 * @param x -- current x position
 * @param y -- current y position
 */
char solveDOWN(char level, char x, char y);

/**
 * Checks if the target's intended new position is used by the map drawing formula.
 * If the new position belongs to a wall, function returns a 1, which forces the
 * MCC_MoveTile function to return without carrying out any motion.
 * @param level -- current level
 * @param x -- current x position
 * @param y -- current y position
 */
char solveLEFT(char level, char x, char y);

/**
 * Checks if the target's intended new position is used by the map drawing formula.
 * If the new position belongs to a wall, function returns a 1, which forces the
 * MCC_MoveTile function to return without carrying out any motion.
 * @param level -- current level
 * @param x -- current x position
 * @param y -- current y position
 */
char solveRIGHT(char level, char x, char y);

#endif
