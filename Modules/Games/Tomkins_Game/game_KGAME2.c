#include "system.h"
#include "random_int.h"
#include "strings.h"

/**@file
 * @brief Kevin Tomkins Game
 *
 * This module is the Game module created by Kevin of Team R.O.C.K.E.T.\n
 * It is supposed to be a twin directional top down shooter, but the source code had too much latency to be implemented on the MSP430.\n
 * The way the game would work may have been to point in a direction with the i, j, k, and l keys, shoot with the spacebar, and to move
 *  in cardinal directions with the a, s, d, and w keys.  Thus, the "twin directions".\n
 *  In the completed game, there would have been two players on two different UART channels.
 *  Players could aim in 8 directions and shoot up to 5 bullets at a time. There would be zombies denoted on the map by a 'Z' character.
 *  A player would be identified by their respective number, 1 or 2.\n
 *  Future versions of the game should have had obstacles such as walls, but that was a second order idea.
 *
 *  @author Kevin Tomkins
 *  @warning This game does not function as intended at normal clock speeds and baud rates.
 * @{
 */

/**Defines the total width of the playing area*/
#define MAP_TOTAL_WIDTH 30

/**Defines the total height of the playing area*/
#define MAP_TOTAL_HEIGHT 20

/**Defines the width of the map that the player will see. */
#define MAP_DRAWN_WIDTH 15

/**Defines the height of the map that the player will see. */
#define MAP_DRAWN_HEIGHT 10


/**Defines the number of lives the players have.*/
#define PLAYER_LIVES 5

/**Defines the number of bullets that the player can have on the map at one time.*/
#define PLAYER_MAX_BULLETS 5

/**Defines how long the bullets will last for (how many times they can move)*/
#define BULLET_LIFE 10

/**Defines the character used for the bullet.*/
#define BULLET_CHAR 253

/**Defines the time in milliseconds for the period of the screen updating*/
#define UPDATE_TIMING 200

/**Defines the period of time which will elapse between the event that move bullets*/
#define FIRE_SPEED 200

/**Defines how fast the player can move in the same way as for bullets*/
#define PLAYER_SPEED 500

/**Defines how quickly zombies can move.*/
#define ZOMBIE_SPEED 8000


/**Defines the chance that a zombie will "spawn" when the map is initialized.
 * A random number will be generated and if it is smaller than this, it will place a zombie*/
#define ZOMBIE_CHANCE 20

/**Defines the fact that the game is using zombies (for testing purposes) and if it is
 * defined, the functions and variables pertinent to zombies will be available to the compiler.
 * Also, defined to be the maximum number of zombies on the map*/
#define MAX_ZOMBIES 1

/**Defines whether the bullets will be scheduled individually or en masse*/
#define MOVE_BULLETS

/**@struct K_game_t Kevin's Game struct.
 *
 * Holds the game variables score and id.\n
 * members:\n
 * score The game score
 * id The ID given to the game by the game subsystem
 *
 */
struct K_game_t {
	uint8 score;
	uint8 id;
};

/**@struct player A player in the game
 * Created to simplify getting information pertinent to one player
 */
struct player {
	uint8 x; ///< x is the player's x position
	uint8 y; ///< y is the player's y position (from the top left corner being 0)
	uint8 direction; ///< direction specifies the N, NE, E, SE, S, SW, W, or NW direction the player's gun is currently pointing in
	uint8 lives; ///< lives is the number of lives the player still has.  This functionality was not introduced.
	uint8 oplx; ///< the other player's last known x coordinate
	uint8 oply; ///< the other player's last known y coordinate
	uint8 lastfire; ///< the time of the last instant the player fired thier gun
};

/**@struct zombie Struct to remember details about zombies in the game
 * The struct contains x and y coordinates, and a marker of them being alive or not
 */
struct zombie {
	uint8 x;
	uint8 y;
	uint8 alive;
};

/**@struct bullet A stuct to hold details about the position, direction and life of a bullet.
 *
 * The standard inclusions of x, y and direction are there.  All others are documented below.
 *
 */
struct bullet {
	uint8 x;
	uint8 y;
	uint8 direction;
	uint8 life; ///< How much life the bullet has before disappearing
	uint8 lastx[2]; ///< The last x coordinate the bullet had for both players
	uint8 lasty[2]; ///< The last y coordinate the bullet had for both players
};

/**
 * The global structs are initialized in the namespace of the whole file.
 */
static struct K_game_t game;
static struct player players[2];
#ifdef MAX_ZOMBIES
static struct zombie zombies[MAX_ZOMBIES];
static uint8 Printed_Z[2][MAP_DRAWN_WIDTH][MAP_DRAWN_HEIGHT];
#endif
static struct bullet bullets[2*PLAYER_MAX_BULLETS];


/**brief Reciver 1 is the UART reciever
 * Processes the incoming characters from the UART channel for player one.\n
 * The same goes for Reciever 2 but for player 2.
 * @param c The current character incoming
 */
static void Receiver1(char c);
#ifdef PLAYER2_UART
static void Receiver2(char c);
#endif

/**@brief Callback for the game subsystem
 * Processes an incoming message from the user and acts appropriately to give the user output.
 * @param argc the number of strings input
 * @param argv an array of pointers to chars for the function to use for the processing of input
 */
static void Callback(int argc, char * argv[]);

/**@brief PrePlay - called for the preprocessing the game requires after initialization
 * PrePlay shows the user instructions to play the game, counts down the timer before the game begins (along with PrePlay2 which is not documented), and schedules the Play funtion to be called.
 */
static void Pre_Play(void);
static void Pre_Play2(void);

/**@brief Play - the processing needed to set up the game before the scheduled game tasks take over
 * Play will check that the game has not yet been run, and reset the necessary functions if it has.
 * It then resets the screen, draws a border, schedules the screen to begin updating, and if the game
 * is set to move bullets en masse it will schedule them to do so.
 */
static void Play(void);
static void Help(void);

/**@brief ends the game
 * resets the variables and displays a game over message.  It gets called when the player comes in
 * contact with a zombie and has no lives left (lives were not successfully introduced, however)
 */
static void Game_Over(void);

/**@brief Updates the screen to place characters where there are players, zombies, or bullets.
 * The update draw will call the functions view_map_update and move_zombies in order to update
 * the players' screens
 */
static void Update_Draw(void);

/**@brief initializes the whole playing area
 * Moves through the whole of the playable area and has the chance to place a zombie based on a random number.
 * Then initializes each player's account of where the zombies are on their screen to all 0's to be filled later
 */
static void Full_Map_Init(void);

/**@brief Prints the maps to the UART channels of each player
 * Runs through the area of the whole game surrounding each player and writes the character for the item there to their UART channel.
 */
static void View_Maps_Print(void);

#ifdef MAX_ZOMBIES
/**
 * Register zombie to the map at a certain position, for a certain zombie
 * @param xpos The x coordinate
 * @param ypos The y coordinate
 * @param num  The number of the beast (negative number indicates a new zombie, and -2 means to reset the automatic index)
 */
static uint8 Register_Zombie(uint8 xpos, uint8 ypos, int8_t num);

/**Returns the number of zombies at the given coordinates*/
static uint8 Find_Zombies_XY(uint8 x, uint8 y);

/**Returns the index of the first zombie in the zombie array which is at the given coordinates*/
static uint8 Zombie_At_XY(uint8 x, uint8 y);

/**Moves the zombies in sequence*/
static void Move_Zombies(void);

/**Kills the zombie who has the given index*/
static void Kill_Zombie(uint8 zombie);

/**For a given player, increases the amount of zombies known at given coordiates */
static void Z_Printed_At(uint8 player, uint8 x, uint8 y);

/**Removes all the zombies from their last known locations on each player's screen*/
static void UnPrint_Zombies(void);

/**Makes the given bullet kill the given zombie, destroying both of them*/
static void Bullet_Kill_Zombie(uint8 bullet, uint8 zombie);
#endif

/**Moves the specified player in a given direction*/
static void Move_Player(char direction, uint8 player); // dir 0 to 7, plr 0 or 1

/**initializes the arrays for both players, their bullets, and all of the zombies.
 * Gets called before the whole map is initialized.*/
static void Initialize_Players(void);

/**Draws each player on the screen of the other, as well as their guns' directions*/
static void Draw_Players(void);

/**Prints a given character on the given player's screen at given coordinates*/
static void Player_Print_XY(char print, uint8 player, uint8 x, uint8 y);

/**@brief Changes the direction of the player
 *
 * Changes the direction that the player is pointing their gun in based on the given input direction.
 *
 * @param direction Is either j, k, l or i and the function changes the given player's direction to it or if it is 90 degrees away from their current location it moves the player's direction to between the two.
 * @param player the given player
 * */
static void Change_Direction(char direction, uint8 player);

/**Fires the bullets for the given player in the direction they are pointing*/
static void Fire(uint8 player); // 0 or 1

/**Moves a given bullet in the direction the bullet is facing.*/
static void Move_Bullet(void * ibullet);

#ifdef MOVE_BULLETS
/**Moves all of the bullets in each of the directions they are facing.*/
static void Move_Bullets(void);
#endif

/**@brief checks if there are bullets at given coordinates
 * If there is a bullet at the given coordinates, returns the index of that bullet.
 *
 * @return The index of the bullet at given coordinates
 */
static uint8 Bullet_At_XY(uint8 x, uint8 y);

/**Initializes the game variables.  Should be defined in system.h or another header file.*/
void KGAME2_Init(void) {
    // Register the module with the game system
    game.id = Game_Register("KGAME", "2D twin directional zombie shooter", Pre_Play, Help);
#ifdef PLAYER2_UART
    Game_EnableMultiPlayer(game.id, 2);
#endif
    // Register a callback with the game system.
    // this is only needed if your game supports more than "play", "help" and "highscores"
    Game_RegisterCallback(game.id, Callback);
}

// recievers
void Receiver1(char c) {
    switch (c) {
        case 'w':
        case 'W':
            Move_Player('w', 0);
            break;
        case 'a':
        case 'A':
        	Move_Player('a', 0);
            break;
        case 's':
        case 'S':
			Move_Player('s', 0);
            break;
        case 'd':
        case 'D':
        	Move_Player('d', 0);
            break;
        case 'i':
        case 'I':
        	Change_Direction('i', 0);
        	break;
        case 'j':
        case 'J':
        	Change_Direction('j', 0);
        	break;
        case 'k':
        case 'K':
        	Change_Direction('k', 0);
        	break;
        case 'l':
        case 'L':
        	Change_Direction('l', 0);
        	break;
        case ' ':
        	if(TimeSince(players[0].lastfire) > FIRE_SPEED){
        		Fire(0);
            	players[0].lastfire = TimeNow();
        	}
        	break;
        default:
            break;
    }
}

#ifdef PLAYER2_UART
void Receiver2(char c) {
    switch (c) {
        case 'w':
        case 'W':
            Move_Player('w', 1);
            break;
        case 'a':
        case 'A':
        	Move_Player('a', 1);
            break;
        case 's':
        case 'S':
			Move_Player('s', 1);
            break;
        case 'd':
        case 'D':
        	Move_Player('d', 1);
            break;
        case 'i':
        case 'I':
        	Change_Direction('i', 1);
        	break;
        case 'j':
        case 'J':
        	Change_Direction('j', 1);
        	break;
        case 'k':
        case 'K':
        	Change_Direction('k', 1);
        	break;
        case 'l':
        case 'L':
        	Change_Direction('l', 1);
        	break;
        case ' ':
        	if(TimeSince(players[1].lastfire) > FIRE_SPEED){
        		Fire(1);
        		players[1].lastfire = TimeNow();
        	}
        	break;
        default:
            break;
    }
}
#endif

// Set up functions
void Callback(int argc, char * argv[]) {
    // "play" and "help" are called automatically so just process "reset" here
    if(argc == 0) Game_Log(game.id, "too few args");
    if(strcasecmp(argv[0],"reset") == 0) {
        Game_Log(game.id, "reset");
    }else Game_Log(game.id, "cmd err");
}

void Pre_Play(void) {
	Task_Schedule(Pre_Play2, 0, 500, 1000);
	//Game_Printf("\fUse WASD to move, IJKL tap to aim, and spacebar to shoot.\r\n"
    //"Game will begin in 10 seconds.\n\r");
    Game_Player1Printf("Player 1");
#ifdef PLAYER2_UART
    Game_Player2Printf("Player 2");
#endif
    Initialize_Players();
	Full_Map_Init();
	players[0].lastfire = 0;
	players[1].lastfire = 0;
}

void Pre_Play2(void) {
	static int i = 10;
	if(i > 0) {
		Game_Printf("\fUse WASD to move, IJKL tap to aim, and spacebar to shoot.\r\n"
	            "Game will begin in %d seconds.\n\r", i);
	}else{
		Task_Remove(Pre_Play2, 0);
		Play();
	}
    i = Mod(i - 1, 10);
}

void Play(void) {
	static uint8 run_before = 0;
	if(run_before){
		Task_Remove(Update_Draw, 0);
#ifndef MOVE_BULLETS
		Task_Remove(Move_Bullet, 0);
#else
		Task_Remove(Move_Bullets, 0);
#endif
		//
#ifdef MAX_ZOMBIES
		Register_Zombie(0, 0, -2);
#endif
		Move_Player(0, 2);
		Fire(2);
	}
	run_before = 1;

	Game_Bell();
    Game_ClearScreen();
    Game_HideCursor();

    Game_RegisterPlayer1Receiver(Receiver1);
#ifdef PLAYER2_UART
    Game_RegisterPlayer2Receiver(Receiver2);
#endif

    Game_DrawRect(0, 0, MAP_DRAWN_WIDTH + 1, MAP_DRAWN_HEIGHT + 1);

    Task_Schedule(Update_Draw, 0, 0, UPDATE_TIMING);

#ifdef MOVE_BULLETS
    Task_Schedule(Move_Bullets, 0, 0, FIRE_SPEED);
#endif
}

void Help(void) { Game_Printf("\fUse WASD to move, IJKL to aim, and spacebar to shoot.\r\n"); }

//Game functions
void Game_Over(void) {
	//stop the game from drawing
    Task_Remove(Update_Draw, 0);
	//stop any bullets
#ifndef MOVE_BULLETS
	Task_Remove(Move_Bullet, 0);
#else
	Task_Remove(Move_Bullets, 0)
#endif
#ifdef MOVE_BULLETS
	Task_Remove ( Move_Bullets, 0 );
#endif
    //print game over and the score
	Game_CharXY(' ', ((MAP_DRAWN_WIDTH / 2) - 5), MAP_DRAWN_HEIGHT/2 + 1);
	Game_Printf("Game Over ");
	Game_CharXY(' ', ((MAP_DRAWN_WIDTH / 2) - 5), MAP_DRAWN_HEIGHT/2 + 2);
	Game_Printf("Score: %d", game.score);
    Game_CharXY('\r', 0, MAP_DRAWN_HEIGHT + 1);

    // unregister the receiver used to run the game
    Game_UnregisterPlayer1Receiver(Receiver1);
#ifdef PLAYER2_UART
    Game_UnregisterPlayer2Receiver(Receiver2);
#endif
    // show cursor (it was hidden at the beginning)
    Game_ShowCursor();
}

void Update_Draw(void){
#ifdef MAX_ZOMBIES
	//last time the zombies moved
	static uint8 zlast_move = 0;
	if(TimeSince(zlast_move) > ZOMBIE_SPEED){
		//get rid of the old zombies
		UnPrint_Zombies();
		//move the zombies
		Move_Zombies();
		zlast_move = TimeNow();
	}
#endif
	//print each player's view map, including zombies and other players
	View_Maps_Print();
	//draw each player above the view map
	Draw_Players();
}

void Full_Map_Init(void){
	//overall x index, y index, player index, and temporary boolean
	uint8 xindex, yindex;
#ifdef MAX_ZOMBIES
	uint8 player, yes;
#endif
	//log
	Game_Printf("\r\nInit2 entered");
	//character to add to the full map
	//iterate through every map tile
	for(xindex = 0; xindex < MAP_TOTAL_WIDTH; xindex++){
		for(yindex = 0; yindex < MAP_TOTAL_HEIGHT; yindex++){
#ifdef MAX_ZOMBIES
			//check if we should place a zombie
			if(random_int(0, 100) < ZOMBIE_CHANCE){
				//for each player...
				for(player = 0; player < 2; player++){
					yes = 0; //...assume we can't place the zombie
							 //there because there is a player, but...
					if(players[player].x != xindex || players[player].y != yindex){
						//...if not, then it is possible to place a zombie
						yes = 1;
					}
				}
				if(yes){
					//register a new zombie here
					Register_Zombie(xindex, yindex, -1);
				}

			}else{
			}
#endif
		}
	}
	//log
	Game_Printf("\r\nFull Map Initialized.\n\r");
#ifdef MAX_ZOMBIES
	//iterate through the players
	for(player = 0; player < 2; player++){
		//iterate through the buffer of where there are zombies printed
		for(xindex = 0; xindex < MAP_DRAWN_WIDTH; xindex++){
			for(yindex = 0; yindex < MAP_DRAWN_HEIGHT; yindex++){
				//log
				Game_Printf("%d\r", xindex);
				//place a zero placeholder
				Printed_Z[player][xindex][yindex] = 0;
			}
		}
	}
	//log
	Game_Printf("\r\nZPrint pointers initialized");
#endif
}

void View_Maps_Print(void){
	//total x and y indices, total x and y initial values, drawable x and y indices
	uint8 xindext, yindext, xindext_init, yindext_init, xindexd, yindexd;
	//player indices, a zombie and bullet placeholder, and temporary boolean
	uint8 player, player2, bhere, yes;
#ifdef MAX_ZOMBIES
	uint8 zhere;
#endif


	//iterate through the players
	for(player = 0; player < 2; player++){
		//choose the opposite player for player2
		player ? (player2 = 0) : (player2 = 1);
		yes = 1; //look for the other player when printing

		//initialize total indices
		xindext_init = Mod(players[player].x - MAP_DRAWN_WIDTH/2, MAP_TOTAL_WIDTH);
		yindext_init = Mod(players[player].y - MAP_DRAWN_HEIGHT/2, MAP_TOTAL_HEIGHT);
		//
		Player_Print_XY(' ', player, players[player].oplx, players[player].oply);
		//
		xindext = xindext_init;
		//for the whole drawing area, offset by one character
		for(xindexd = 1; xindexd <= MAP_DRAWN_WIDTH; xindexd++){

			yindext = yindext_init; //reset total y index
			//for the whole drawn column
			for(yindexd = 1; yindexd <= MAP_DRAWN_HEIGHT; yindexd++){
#ifdef MAX_ZOMBIES
				//find a zombie by zombie index
				zhere = Zombie_At_XY(xindext, yindext);
				//if there's a zombie
				if(zhere < MAX_ZOMBIES){
					//if it is alive
					if(zombies[zhere].alive) {
						//set the terminal color to green
						Terminal_SetColor(player, ForegroundGreen);
						//print a z for this player
						Player_Print_XY('Z', player, xindexd, yindexd);
						//reset terminal color
						Terminal_SetColor(player, Reset_all_attributes);
						//record the location of the new zombie
						Z_Printed_At(player, xindexd - 1, yindexd - 1);
					}
				}
#endif
				bhere = Bullet_At_XY(xindext, yindext);
				if(bhere < PLAYER_MAX_BULLETS){
					Player_Print_XY(' ', player, bullets[bhere].lastx[player], bullets[bhere].lasty[player]);
					Player_Print_XY(BULLET_CHAR, player, xindexd, yindexd);
					bullets[bhere].lastx[player] = xindexd;
					bullets[bhere].lasty[player] = yindexd;
				}

				//if we are checking for the other player
				// and if we are at that player's location
				if(yes && players[player2].x == xindext && players[player2].y == yindext){
					//print a P
					Player_Print_XY((player2 ? '2' : '1'), player, xindexd, yindexd);
					players[player].oplx = xindexd;
					players[player].oply = yindexd;
					yes = 0; //stop looking for the other player
				}
				yindext++;
			}
			xindext++;
		}
	}
}

// Zombie functions
#ifdef MAX_ZOMBIES
uint8 Register_Zombie(uint8 xpos, uint8 ypos, int8_t num){
	//zombie index for each new zombie
	static uint8 zomb_index = 0;
	//if the index is out of range
	if(num > MAX_ZOMBIES || zomb_index > MAX_ZOMBIES){
		Game_Log(game.id, "ZR err");
		return MAX_ZOMBIES; //show that we have failed
	}
	if(num < 0){ //the range should be the ongoing index
		if(num == -2){
			//special case to reset the index
			zomb_index = 0;
			return 0;
		}
		//set coordinates for the zombie at this index
		zombies[zomb_index].x = xpos;
		zombies[zomb_index].y = ypos;
		zombies[zomb_index].alive = 1; //set them alive
		//increase the ongoing index
		zomb_index = (zomb_index + 1) % MAX_ZOMBIES;
		//return the index that was just assigned
		return zomb_index - 1;
	}else{ //set to the index of the parameter
		zombies[num].x = xpos;
		zombies[num].y = ypos;
		zombies[num].alive = 1;
		return num;
	}
}

uint8 Find_Zombies_XY(uint8 x, uint8 y){
	//index - unsigned to increase speed
	int8_t i;
	//number of zombies at these coordinates
	uint8 zombie = 0;
	//for all zombies
	for(i = MAX_ZOMBIES; i >= 0; i--){
		//if the zombie's coordinates are the parameters
		if(zombies[i].x == x && zombies[i].y == y){
			zombie++; //increase the number here
		}
	}
	return zombie; //return that number
}

uint8 Zombie_At_XY(uint8 x, uint8 y){
	//index - unsigned because we count up here
	uint8 i;
	//for all zombies
	for(i = 0; i < MAX_ZOMBIES; i++){
		//find the first zombie at this space
		if(zombies[i].x == x && zombies[i].y == y){
			return i; //return that zombie's index
		}
	}
	//otherwise show it contains no zombies by an out of range value
	return MAX_ZOMBIES;
}

void Move_Zombies(void){
	//zombie index, x and y indices, and a random player placeholder
	uint8 zomb_index, x, y, rand_plr;
	//player coordinates
	uint8 px, py;

	//for all zombies
	for(zomb_index = 0; zomb_index < MAX_ZOMBIES; zomb_index++){
		//if the zombie is not alive, create a new zombie
		if(!zombies[zomb_index].alive){
			x = random_int(0, MAP_TOTAL_WIDTH - 1);
			y = random_int(0, MAP_TOTAL_HEIGHT - 1);
			Register_Zombie(x, y, zomb_index);
		}else{
			//otherwise, use x and y as the current coordinates
			x = zombies[zomb_index].x;
			y = zombies[zomb_index].y;
		}
		//rand_plr = random_int(0, 1);
		rand_plr = 0;
		px = players[rand_plr].x;
		py = players[rand_plr].y;

		if(x > px){
			x--;
		}else{
			if(x < px){
				x++;
			}
		}
		zombies[zomb_index].x = x;

		if(y > py){
			y--;
		}else{
			if(y < py){
				y++;
			}
		}
		zombies[zomb_index].y = y;
		if(y == players[0].y && x == players[0].x){
			Game_Over();
		}
#ifdef PLAYER2_UART
		if(y == players[1].y && x == players[1].x){
			Game_Over();
		}
#endif
		uint8 bull = Bullet_At_XY(x, y);
		if(bull < 2*PLAYER_MAX_BULLETS){
			Bullet_Kill_Zombie(bull, zomb_index);
		}
	}
}

void Kill_Zombie(uint8 zombie){
	if(zombie < MAX_ZOMBIES) zombies[zombie].alive = 0;
}

void Z_Printed_At(uint8 player, uint8 x, uint8 y){
	Printed_Z[player][x][y]++;
}

void UnPrint_Zombies(void){
	uint8 xindex, yindex, player;
	for(player = 0; player < 2; player++){
		for(xindex = 1; xindex <= MAP_DRAWN_WIDTH; xindex++){
			for(yindex = 1; yindex <= MAP_DRAWN_HEIGHT; yindex++){
				if(Printed_Z[player][xindex - 1][yindex - 1]){
					Player_Print_XY(' ', player, xindex, yindex);
					Printed_Z[player][xindex - 1][yindex - 1] = 0;
				}
			}
		}
	}
}

void Bullet_Kill_Zombie(uint8 bullet, uint8 zombie){
	zombies[zombie].alive = 0;
	bullets[bullet].life = 0;
	game.score++;
}
#endif

// Player functions
void Move_Player(char direction, uint8 player){
	static uint32_t lastmove[2] = {0, 0};
	if(player > 1){
		lastmove[0] = 0;
		lastmove[1] = 0;
	}
	if(TimeSince(lastmove[player]) >= PLAYER_SPEED){
		lastmove[player] = TimeNow();
		switch(direction){
			case 'w':
				players[player].y = Mod(players[player].y - 1, MAP_TOTAL_HEIGHT);
				break;
			case 'a':
				players[player].x = Mod(players[player].x - 1, MAP_TOTAL_WIDTH);
				break;
			case 's':
				players[player].y = (players[player].y + 1) % MAP_TOTAL_HEIGHT;
				break;
			case 'd':
				players[player].x = (players[player].x + 1) % MAP_TOTAL_WIDTH;
				break;
			default:
				Game_Log(game.id, "MP err");
				break;
		}
#ifdef MAX_ZOMBIES
		if(Find_Zombies_XY(players[player].x, players[player].y)){
			Game_Over();
		}
#endif
		Player_Print_XY('\r', player, 0, MAP_DRAWN_HEIGHT + 2);
		player ? Game_Player2Printf("Player 2 x: %u, y: %u  ", players[player].x, players[player].y) : Game_Player1Printf("Player 1 x: %u, y: %u  ", players[player].x, players[player].y);
	}
}

void Initialize_Players(void){
	uint8 index, x, y;
	game.score = 0;
	Game_Printf("\n\rInit 1 entered");
	for(index = 0; index < 2; index++){
		x = random_int(0, MAP_TOTAL_WIDTH);
		players[index].x = x;
		Game_Printf("\n\rSet player %d x pos to: %d", index + 1, x);

		y = random_int(0, MAP_TOTAL_HEIGHT);
		players[index].y = y;
		Game_Printf("\n\rSet player %d y pos to: %d", index + 1, y);

		for(x = 0; x < 2*PLAYER_MAX_BULLETS; x++){

			bullets[x].life = 0;
		}

	}

	if(players[0].x == players[1].x){
		players[1].x = (players[1].x + 1) % MAP_TOTAL_WIDTH;
	}

	if(players[0].y == players[1].y){
		players[1].y = (players[1].y + 1) % MAP_TOTAL_HEIGHT;
	}
	Game_Printf("\r\nPlayers Initialized.");
#ifdef MAX_ZOMBIES
	for(index = MAX_ZOMBIES; index > 0; index--){
		zombies[index - 1].x = index;
		zombies[index - 1].y = 0;
		zombies[index - 1].alive = 0;
	}
	Game_Printf("\r\nZombies Initialized.");
#endif
}

void Draw_Players(void){
	//Game_CharXY(' ', MAP_DRAWN_WIDTH/2 - 1, MAP_DRAWN_HEIGHT/2 - 1);
	//Game_Printf("  \n%c%c%c\n   ");
	uint8 player, x, y;
	char print;
	for(player = 0; player < 2; player++){
		x = MAP_DRAWN_WIDTH/2;
		y = MAP_DRAWN_HEIGHT/2;
		Player_Print_XY((player ? '2' : '1'), player, x, y);
		switch(players[player].direction){
		case 0:
			print = '|';
			y--;
			break;
		case 1:
			print = '/';
			x++;
			y--;
			break;
		case 2:
			print = '-';
			x++;
			break;
		case 3:
			print = '\\';
			x++;
			y++;
			break;
		case 4:
			print = '|';
			y++;
			break;
		case 5:
			print = '/';
			x--;
			y++;
			break;
		case 6:
			print = '-';
			x--;
			break;
		case 7:
			print = '\\';
			x--;
			y--;
			break;
		default:
			print = ' ';
			break;
		}
		Player_Print_XY(print, player, x, y);
	}
}

void Player_Print_XY(char print, uint8 player, uint8 x, uint8 y){
	if(player){
#ifdef PLAYER2_UART
		Game_Player2CharXY(print, x, y);
#endif
	}
	else{
		Game_Player1CharXY(print, x, y);
	}
}

void Change_Direction(char direction, uint8 player){
	uint8 x, y;
	x = players[player].x;
	y = players[player].y;
	switch(players[player].direction){
	case 0:
		y--;
		break;
	case 1:
		x++;
		y--;
		break;
	case 2:
		x++;
		break;
	case 3:
		x++;
		y++;
		break;
	case 4:
		y++;
		break;
	case 5:
		x--;
		y++;
		break;
	case 6:
		x--;
		break;
	case 7:
		x--;
		y--;
		break;
	default:
		break;
	}
	Player_Print_XY(' ', player, x, y);
	//
	//0-i, 1-il, 2-l, 3-kl, 4-k, 5-jk, 6-j, 7-ij
	switch(players[player].direction){
		case 0: //i
			switch(direction){
				case 'i':
					break;
				case 'j':
					players[player].direction = 7;
					break;
				case 'k':
					players[player].direction = 4;
					break;
				case 'l':
					players[player].direction = 2;
					break;
			}
			break;
		case 2: //l
			switch(direction){
				case 'i':
					players[player].direction = 1;
					break;
				case 'j':
					players[player].direction = 6;
					break;
				case 'k':
					players[player].direction = 3;
					break;
				case 'l':
					break;
			}
			break;
		case 4: //k
			switch(direction){
				case 'i':
					players[player].direction = 0;
					break;
				case 'j':
					players[player].direction = 5;
					break;
				case 'k':
					break;
				case 'l':
					players[player].direction = 3;
					break;
			}
			break;
		case 6: //j
			switch(direction){
				case 'i':
					players[player].direction = 7;
					break;
				case 'j':
					break;
				case 'k':
					players[player].direction = 5;
					break;
				case 'l':
					players[player].direction = 2;
					break;
			}
			break;
		default:
			switch(direction){
				case 'i':
					players[player].direction = 0;
					break;
				case 'j':
					players[player].direction = 6;
					break;
				case 'k':
					players[player].direction = 4;
					break;
				case 'l':
					players[player].direction = 2;
					break;
			}
			break;
	}
}

void Fire(uint8 player){
	static uint8 bull_index = 0;
	if(player > 1){
		bull_index = 0;
	}
	int8_t x, y;
	uint8 playerx, playery, direction, ibullet;
#ifdef MAX_ZOMBIES
	uint8 zombie;
#endif
	playerx = players[player].x;
	playery = players[player].y;
	direction = players[player].direction;
	bullets[bull_index].direction = direction;
	bullets[bull_index].life = BULLET_LIFE;
	switch(direction){
		case 0:
			x = playerx;
			y = playery - 1;
			break;
		case 1:
			x = playerx + 1;
			y = playery - 1;
			break;
		case 2:
			x = playerx + 1;
			y = playery;
			break;
		case 3:
			x = playerx + 1;
			y = playery + 1;
			break;
		case 4:
			x = playerx;
			y = playery + 1;
			break;
		case 5:
			x = playerx - 1;
			y = playery + 1;
			break;
		case 6:
			x = playerx - 1;
			y = playery;
			break;
		case 7:
			x = playerx - 1;
			y = playery - 1;
			break;
		default:
			Game_Log(game.id, "BL err");
			break;
	}
	bullets[bull_index].x = Mod(x, MAP_TOTAL_WIDTH);
	bullets[bull_index].y = Mod(y, MAP_TOTAL_HEIGHT);
	bull_index = (bull_index + 1) % PLAYER_MAX_BULLETS;
#ifdef MAX_ZOMBIES
	zombie = Zombie_At_XY(x, y);
	if(zombie < MAX_ZOMBIES){
		Kill_Zombie(zombie);
		game.score++;
		bullets[bull_index].life = 0;
	}
#endif
	ibullet = (player + 1) << 4;
	ibullet += bull_index + 1;
	Player_Print_XY('\r', player, 0, MAP_DRAWN_HEIGHT + 3);
	player ? Game_Player2Printf("Fired bullet %x in direction: %d", ibullet, direction) : Game_Player1Printf("Fired bullet %x in direction: %d", ibullet, direction);
	void * ibullet2 = (void *)ibullet;
#ifndef MOVE_BULLETS
	Task_Schedule(Move_Bullet, ibullet2, 0, FIRE_SPEED);
#endif
}

#ifndef MOVE_BULLETS
void Move_Bullet(void * ibullet2){
	uint8 index, x, y, direction, ibullet, player;
#ifdef MAX_ZOMBIES
	uint8 zombie;
#endif
	ibullet = (uint8_t) ibullet2;
	player = ibullet & 0xF0;
	index = ibullet & 0x0F;
	index--;
	if(!(bullets[index].life)){
		Task_Remove(Move_Bullet, ibullet2);
		return;
	}
	direction = bullets[index].direction;
	Player_Print_XY('\r', player, 0, MAP_DRAWN_HEIGHT + 4);
	player ? Game_Player2Printf("Mov b %x dir %d i", ibullet, direction, index) : Game_Player1Printf("Mov b %x dir %d", ibullet, direction);
	x = bullets[index].x;
	y = bullets[index].y;
	switch(direction){
		case 0:
			y--;
			break;
		case 1:
			y--;
			x++;
			break;
		case 2:
			x++;
			break;
		case 3:
			x++;
			y++;
			break;
		case 4:
			y++;
			break;
		case 5:
			y++;
			x--;
			break;
		case 6:
			x--;
			break;
		case 7:
			x--;
			y--;
			break;
		default:
			Game_Log(game.id, "BD err");
			break;
	}
				//
	if(x > MAP_TOTAL_WIDTH) x = x % MAP_TOTAL_WIDTH;
	if(y > MAP_TOTAL_HEIGHT) y = y % MAP_TOTAL_HEIGHT;
	bullets[index].x = x;
	bullets[index].y = y;
	bullets[index].life--;
#ifdef MAX_ZOMBIES
	zombie = Zombie_At_XY(x, y);
	if(zombie < MAX_ZOMBIES){
		Bullet_Kill_Zombie(index, zombie);
	}
#endif
	return;
}
#endif

#ifdef MOVE_BULLETS
void Move_Bullets(void){
	uint8_t index, x, y, direction, player
#ifdef MAX_ZOMBIES
	uint8 zombie;
#endif
	for(index = 0; index < 2*PLAYER_MAX_BULLETS; index++){
		direction = bullets[index].direction;
		x = bullets[index].x;
		y = bullets[index].y;
		//
		switch(direction){
			case 0:
				y--;
				break;
			case 1:
				y--;
				x++;
				break;
			case 2:
				x++;
				break;
			case 3:
				x++;
				y++;
				break;
			case 4:
				y++;
				break;
			case 5:
				y++;
				x--;
				break;
			case 6:
				x--;
				break;
			case 7:
				x--;
				y--;
				break;
			default:
				Game_Log(game.id, "BD err");
				break;
		}
		//
		if(x > MAP_TOTAL_WIDTH) x = x % MAP_TOTAL_WIDTH;
		if(y > MAP_TOTAL_HEIGHT) y = y % MAP_TOTAL_HEIGHT;
		//
#ifdef MAX_ZOMBIES
		zombie = Zombie_At_XY(x, y);
		if(zombie < MAX_ZOMBIES){
			Bullet_Kill_Zombie(index, zombie);
		}
#endif
		bullets[index].x = x;
		bullets[index].y = y;
	}
}
#endif

uint8 Bullet_At_XY(uint8 x, uint8 y){
	uint8 index;
	for(index = 0; index < 2*PLAYER_MAX_BULLETS; index++){
		if(bullets[index].x == x && bullets[index].y == y){
			if(bullets[index].life > 0) return index;
		}
	}
	return 2*PLAYER_MAX_BULLETS;
}

/** @}/
